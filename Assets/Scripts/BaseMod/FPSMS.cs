﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class FPSMS
{ 
    private static float deltaTime = 0.0f;
    public static void Update(mGraphics g)
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        float ms = deltaTime * 1000.0f;
        mFont.tahoma_7b_red.drawString(g, "FPS: " + (int)fps, 85, 30, mFont.LEFT, mFont.tahoma_7b_dark);
        if (ms < 40)
        {
            mFont.tahoma_7_green.drawString(g, (int)ms + "ms", 120, 30, mFont.LEFT, mFont.tahoma_7b_dark);
        }else if (ms < 100)
        {
            mFont.tahoma_7_yellow.drawString(g, (int)ms + "ms", 120, 30, mFont.LEFT, mFont.tahoma_7b_dark);
        }else
        {
            mFont.tahoma_7_red.drawString(g, (int)ms + "ms", 120, 30, mFont.LEFT, mFont.tahoma_7b_focus);
        }
        
    }
}

