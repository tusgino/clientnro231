using UnityEngine;


public class AutoBoss
{
	public static bool doBoss;

	public static int zoneMacDinh;

	public static string bossCanDo;

	private static long currDoBoss;

	public static void DoBoss()
	{
		if (string.IsNullOrEmpty(bossCanDo))
		{
			GameScr.info1.addInfo("Chưa nhập boss cần tìm", 0);
			zoneMacDinh = 0;
			doBoss = false;
			return;
		}
		if (Input.GetKey("q"))
		{
			GameScr.info1.addInfo("Đã tắt auto dò boss", 0);
			doBoss = false;
			return;
		}
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (@char != null && @char.cName.ToLower().Contains(bossCanDo.ToLower()) && @char.cTypePk == 5)
			{
				Sound.start(1f, Sound.l1);
				GameScr.info1.addInfo("Đã tìm thấy boss", 0);
				zoneMacDinh = 0;
				doBoss = false;
				return;
			}
		}
		Service.gI().requestChangeZone(zoneMacDinh, -1);
		if (!Char.isLoadingMap && TileMap.zoneID == zoneMacDinh)
		{
			zoneMacDinh++;
		}
	}

	public static void chat(string str)
	{
		if (str.StartsWith("do "))
		{
			bossCanDo = str.Replace("do ", "");
			GameScr.info1.addInfo("Boss cần dò: " + bossCanDo, 0);
			str = "";
		}
		if (str.StartsWith("dk "))
		{
			zoneMacDinh = int.Parse(str.Replace("dk ", ""));
			GameScr.info1.addInfo("Dò boss từ khu " + zoneMacDinh, 0);
			str = "";
		}
		if (str == "clrz")
		{
			zoneMacDinh = 0;
			GameScr.info1.addInfo("Reset khu dò boss xuống", 0);
			str = "";
		}
		if (str == "doall")
		{
			doBoss = !doBoss;
			GameScr.info1.addInfo("Dò boss: " + (doBoss ? "Bật" : "Tắt"), 0);
			str = "";
		}
	}

	public static void Update()
	{
		if (doBoss && mSystem.currentTimeMillis() - currDoBoss >= 1000)
		{
			DoBoss();
			currDoBoss = mSystem.currentTimeMillis();
		}
	}
}
