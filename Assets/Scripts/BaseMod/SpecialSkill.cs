using System;
using System.Threading;

internal class SpecialSkill : IActionListener, IChatable
{
	private int cgender = Char.myCharz().cgender;

	public static string noitai;

	public static string tennoitaicanmo;

	public static sbyte type;

	public static bool openMax;

	public static int max;

	public static int[] chiso;

	private static SpecialSkill instance;

	public bool isnoitai;

	public static SpecialSkill gI()
	{
		if (instance != null)
		{
			return instance;
		}
		return instance = new SpecialSkill();
	}

	public void open()
	{
		if (Char.myCharz().cPower < 10000000000L)
		{
			GameScr.info1.addInfo("Cần 10 tỉ sức mạnh để mở.", 0);
			return;
		}
		if (openMax && max == -1)
		{
			isnoitai = false;
			openMax = false;
			return;
		}
		while (true)
		{
			if (Char.myCharz().luong < 1000)
			{
				if (cgender == 0)
				{
					isnoitai = false;
					Service.gI().openMenu(0);
					Service.gI().confirmMenu(0, 2);
					isnoitai = true;
				}
				if (cgender == 1)
				{
					isnoitai = false;
					Service.gI().openMenu(2);
					Service.gI().confirmMenu(2, 2);
					isnoitai = true;
				}
				if (cgender == 3)
				{
					isnoitai = false;
					Service.gI().openMenu(1);
					Service.gI().confirmMenu(1, 2);
					isnoitai = true;
				}
			}
			Service.gI().speacialSkill(0);
			if (Panel.specialInfo.Contains(tennoitaicanmo))
			{
				if (!openMax)
				{
					isnoitai = false;
					GameScr.info1.addInfo("Xong", 0);
					return;
				}
				int num = Panel.specialInfo.IndexOf("%");
				int num2 = Panel.specialInfo.Substring(0, num).LastIndexOf(' ');
				if (int.Parse(CutString(num2 + 1, num - 1, Panel.specialInfo)) == max)
				{
					break;
				}
			}
			Thread.Sleep(200);
			Service.gI().confirmMenu(5, type);
			Thread.Sleep(200);
			Service.gI().confirmMenu(5, 0);
			Thread.Sleep(400);
			if (!isnoitai)
			{
				return;
			}
		}
		isnoitai = false;
		openMax = false;
		GameScr.info1.addInfo("Xong", 0);
	}

	public string CutString(int start, int end, string s)
	{
		string text = "";
		for (int i = start; i <= end; i++)
		{
			text += s[i];
		}
		return text;
	}

	public void perform(int idAction, object p)
	{
		switch (idAction)
		{
		case 2:
		{
			string text2 = (string)p;
			int length2 = text2.Substring(0, text2.IndexOf('%')).LastIndexOf(' ');
			tennoitaicanmo = text2.Substring(0, length2);
			isnoitai = true;
			type = (sbyte)idAction;
			GameCanvas.panel.hide();
			new Thread(open).Start();
			break;
		}
		case 1:
		{
			string text = (string)p;
			int length = text.Substring(0, text.IndexOf('%')).LastIndexOf(' ');
			tennoitaicanmo = text.Substring(0, length);
			isnoitai = true;
			type = (sbyte)idAction;
			GameCanvas.panel.hide();
			new Thread(open).Start();
			break;
		}
		}
		if (idAction == 3)
		{
			openMax = false;
			MyVector myVector = new MyVector();
			myVector.addElement(new Command("Mở Vip", gI(), 2, p));
			myVector.addElement(new Command("Mở Thường", gI(), 1, p));
			GameCanvas.menu.startAt(myVector, 3);
		}
		if (idAction == 4)
		{
			string obj = (string)p;
			openMax = true;
			int num = obj.IndexOf("đến ");
			int length3 = obj.Substring(num + 4).IndexOf("%");
			max = int.Parse(obj.Substring(num + 4, length3));
			MyVector myVector2 = new MyVector();
			myVector2.addElement(new Command("Mở Vip", gI(), 2, p));
			myVector2.addElement(new Command("Mở Thường", gI(), 1, p));
			GameCanvas.menu.startAt(myVector2, 3);
		}
		if (idAction == 5)
		{
			MyVector myVector3 = new MyVector();
			for (int i = 1; i <= 8; i++)
			{
				myVector3.addElement(new Command(i + " sao", this, 7, i));
			}
			GameCanvas.menu.startAt(myVector3, 3);
		}
		if (idAction == 6)
		{
			Service.gI().combine(1, GameCanvas.panel.vItemCombine);
		}
		if (idAction == 8)
		{
			string text3 = (string)p;
			int length4 = text3.Substring(0, text3.IndexOf('%')).LastIndexOf(' ');
			tennoitaicanmo = text3.Substring(0, length4);
			int num2 = text3.IndexOf("%");
			int num3 = text3.IndexOf("đến ");
			int start = text3.Substring(0, num2).LastIndexOf(' ');
			int num4 = text3.LastIndexOf('%');
			chiso[0] = int.Parse(CutString(start, num2 - 1, text3));
			chiso[1] = int.Parse(CutString(num3 + 4, num4 - 1, text3));
			string text4 = CutString(start, num4, text3);
			noitai = "Nhập chỉ số bạn muốn chọn trong khoảng " + text4;
			MyVector myVector4 = new MyVector();
			myVector4.addElement(new Command("Mở Vip", gI(), 9, 2));
			myVector4.addElement(new Command("Mở Thường", gI(), 9, 1));
			GameCanvas.menu.startAt(myVector4, 3);
		}
		if (idAction == 9)
		{
			type = (sbyte)(int)p;
			startChat(1, noitai);
		}
	}

	public void onChatFromMe(string text, string to)
	{
		if (GameCanvas.panel.chatTField.strChat == noitai)
		{
			int num = -1;
			try
			{
				num = int.Parse(GameCanvas.panel.chatTField.tfChat.getText());
			}
			catch (Exception)
			{
				GameCanvas.startOKDlg(mResources.input_quantity_wrong);
				GameCanvas.panel.chatTField.isShow = false;
				GameCanvas.panel.chatTField.tfChat.setIputType(TField.INPUT_TYPE_ANY);
				return;
			}
			if (num != -1 && num >= chiso[0] && num <= chiso[1])
			{
				max = num;
				openMax = true;
				gI().isnoitai = true;
				GameCanvas.panel.hide();
				new Thread(gI().open).Start();
				GameCanvas.panel.chatTField.isShow = false;
				GameCanvas.panel.chatTField.tfChat.setIputType(TField.INPUT_TYPE_ANY);
			}
			else
			{
				GameCanvas.startOKDlg(noitai);
			}
		}
		GameCanvas.panel.chatTField.parentScreen = GameCanvas.panel;
	}

	public void onCancelChat()
	{
		GameCanvas.panel.chatTField.tfChat.setIputType(TField.INPUT_TYPE_ANY);
	}

	public static void startChat(int type, string caption)
	{
		if (GameCanvas.panel.chatTField == null)
		{
			GameCanvas.panel.chatTField = new ChatTextField();
			GameCanvas.panel.chatTField.tfChat.y = GameCanvas.h - 35 - ChatTextField.gI().tfChat.height;
			GameCanvas.panel.chatTField.initChatTextField();
			GameCanvas.panel.chatTField.parentScreen = gI();
		}
		GameCanvas.panel.chatTField.parentScreen = gI();
		ChatTextField chatTField = GameCanvas.panel.chatTField;
		chatTField.strChat = caption;
		chatTField.tfChat.name = mResources.input_quantity;
		chatTField.to = string.Empty;
		chatTField.isShow = true;
		chatTField.tfChat.isFocus = true;
		chatTField.tfChat.setIputType(type);
		if (GameCanvas.isTouch)
		{
			GameCanvas.panel.chatTField.tfChat.doChangeToTextBox();
		}
		if (!Main.isPC)
		{
			chatTField.startChat2(gI(), string.Empty);
		}
	}

	public void In4Me(string s)
	{
		if (s.ToLower().Contains("bạn không đủ vàng"))
		{
			gI().isnoitai = false;
			openMax = false;
		}
	}

	static SpecialSkill()
	{
		type = 1;
		openMax = false;
		max = -1;
		chiso = new int[2];
	}

	public static void AddChatPopup(string[] s)
	{
		if (s.Length != 0 && s[s.Length - 1] != string.Empty)
		{
			string text = s[s.Length - 1].ToLower();
			int num = text.IndexOf("cần ");
			int num2 = text.IndexOf("tr");
			if (num != -1 && num2 != -1)
			{
				int.Parse(gI().CutString(num + 3, num2 - 1, text).Trim());
			}
		}
	}

	public static void startMenu()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Thường", gI(), 6, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public void onChatFromMee(string text, string to)
	{
		if (GameCanvas.panel.chatTField.strChat == noitai)
		{
			int num = -1;
			try
			{
				num = int.Parse(GameCanvas.panel.chatTField.tfChat.getText());
			}
			catch (Exception)
			{
				GameCanvas.startOKDlg(mResources.input_quantity_wrong);
				GameCanvas.panel.chatTField.isShow = false;
				GameCanvas.panel.chatTField.tfChat.setIputType(TField.INPUT_TYPE_ANY);
				return;
			}
			if (num != -1 && num >= chiso[0] && num <= chiso[1])
			{
				max = num;
				openMax = true;
				gI().isnoitai = true;
				GameCanvas.panel.hide();
				new Thread(gI().open).Start();
				GameCanvas.panel.chatTField.isShow = false;
				GameCanvas.panel.chatTField.tfChat.setIputType(TField.INPUT_TYPE_ANY);
			}
			else
			{
				GameCanvas.startOKDlg(noitai);
			}
		}
		GameCanvas.panel.chatTField.parentScreen = GameCanvas.panel;
	}
}
