using System.Collections.Generic;
using System.Threading;


public class Tugino
{
	public static bool isTDLT;

	public static bool apick;

	public static List<int> itemBlock;

	public static long timepickitem;

	public static List<int> itemPickUp;

	public static bool dangLogin;

	public static bool isAutoTTNL;

	private static long currAutoTTNL;

	public static bool isAutoHS;

	private static long currAutoHS;

	public static bool isKhoaViTri;

	public static int ghimX;

	public static int ghimY;

	private static long currKhoaViTri;

	public static bool isKhoaKhu;

	private static long lastTimeCheckLag;

	public static int currentCheckLag;

	public static bool isCheckLag;

	public static bool isKOK;

	private static long currKOK;

	private static long currAutoFlag;

	public static bool isAutoCo;

	public static int soSao(Item item)
	{
		for (int i = 0; i < item.itemOption.Length; i++)
		{
			if (item.itemOption[i].optionTemplate.id == 107)
			{
				return item.itemOption[i].param;
			}
		}
		return 0;
	}

	public static string saoTrongBalo(Item item)
	{
		if ((item != null && item.template.type <= 5) || item.template.type == 32)
		{
			return string.Concat(soSao(item));
		}
		return "";
	}

	static Tugino()
	{
		currentCheckLag = 30;
		isCheckLag = false;
		itemBlock = new List<int>();
		itemPickUp = new List<int> { 77, 861, 191, 76, 188, 189, 190, 192, 74 };
	}

	public static ItemMap searchItem()
	{
		ItemMap result = null;
		int num = -1;
		for (int i = 0; i < GameScr.vItemMap.size(); i++)
		{
			ItemMap itemMap = (ItemMap)GameScr.vItemMap.elementAt(i);
			int num2 = Math.abs(Char.myCharz().cx - itemMap.x);
			int num3 = Math.abs(Char.myCharz().cy - itemMap.y);
			int num4 = ((num2 > num3) ? num3 : num2);
			bool flag = itemMap.playerId == -1 || itemMap.playerId == Char.myCharz().charID;
			if (itemMap.template.type != 22 && (itemBlock.Count <= 0 || !itemBlock.Contains(itemMap.template.id)))
			{
				if (num == -1 && flag && itemMap.template.type != 33)
				{
					num = num4;
					result = itemMap;
				}
				else if (num4 < num && num4 <= 48 && flag && itemMap.template.type != 33)
				{
					num = num4;
					result = itemMap;
				}
			}
		}
		return result;
	}

	public static bool havePickupItem(ItemMap item)
	{
		if (itemPickUp.Contains(item.template.id))
		{
			return true;
		}
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			Item item2 = Char.myCharz().arrItemBag[i];
			if (Char.myCharz().arrItemBag[i] == null)
			{
				return true;
			}
			if (item2.template.id == item.template.id && item2.quantity < 99)
			{
				return true;
			}
		}
		return false;
	}

	public static int distance(int x1, int y1, int x2, int y2)
	{
		int num = Math.abs(x1 - x2);
		int num2 = Math.abs(y1 - y2);
		if (num <= num2)
		{
			return num;
		}
		return num2;
	}

	public static void TDLT()
	{
		if (GameScr.isChangeZone || Char.myCharz().statusMe == 14 || Char.myCharz().statusMe == 5 || Char.myCharz().isCharge || Char.myCharz().isFlyAndCharge || Char.myCharz().isUseChargeSkill())
		{
			return;
		}
		if (apick)
		{
			ItemMap itemMap = searchItem();
			if (itemMap != null && havePickupItem(itemMap))
			{
				if (mSystem.currentTimeMillis() - timepickitem > 1000)
				{
					if (distance(Char.myCharz().cx, Char.myCharz().cy, itemMap.x, itemMap.y) > 50)
					{
						GameScr.gI().checkClickMoveTo(itemMap.x, itemMap.y);
					}
					else
					{
						Service.gI().pickItem(itemMap.itemMapID);
					}
					timepickitem = mSystem.currentTimeMillis();
				}
				return;
			}
		}
		bool flag = false;
		for (int i = 0; i < GameScr.vMob.size(); i++)
		{
			Mob mob = (Mob)GameScr.vMob.elementAt(i);
			if (mob.status != 0 && mob.status != 1)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			return;
		}
		bool flag2 = false;
		for (int j = 0; j < Char.myCharz().arrItemBag.Length; j++)
		{
			Item item = Char.myCharz().arrItemBag[j];
			if (item != null && item.template.type == 6)
			{
				flag2 = true;
				break;
			}
		}
		if (!flag2 && GameCanvas.gameTick % 150 == 0)
		{
			Service.gI().requestPean();
		}
		if (Char.myCharz().cHP <= Char.myCharz().cHPFull * 20 / 100 || Char.myCharz().cMP <= Char.myCharz().cMPFull * 20 / 100)
		{
			GameScr.gI().doUseHP();
		}
		if (Char.myCharz().mobFocus == null || (Char.myCharz().mobFocus != null && Char.myCharz().mobFocus.isMobMe))
		{
			for (int k = 0; k < GameScr.vMob.size(); k++)
			{
				Mob mob2 = (Mob)GameScr.vMob.elementAt(k);
				if (mob2.status != 0 && mob2.status != 1 && mob2.hp > 0 && !mob2.isMobMe && !mob2.checkIsBoss())
				{
					Char.myCharz().cx = mob2.x;
					Char.myCharz().cy = mob2.y;
					Char.myCharz().mobFocus = mob2;
					Service.gI().charMove();
					Char.myCharz().cx = mob2.x;
					Char.myCharz().cy = mob2.y + 1;
					Service.gI().charMove();
					Char.myCharz().cx = mob2.x;
					Char.myCharz().cy = mob2.y;
					Service.gI().charMove();
					Res.outz("focus 1 con bossssssssssssssssssssssssssssssssssssssssssssssssss");
					break;
				}
			}
		}
		else if (Char.myCharz().mobFocus.hp <= 0 || Char.myCharz().mobFocus.status == 1 || Char.myCharz().mobFocus.status == 0)
		{
			Char.myCharz().mobFocus = null;
		}
		if (Char.myCharz().mobFocus == null || (Char.myCharz().skillInfoPaint() != null && Char.myCharz().indexSkill < Char.myCharz().skillInfoPaint().Length && Char.myCharz().dart != null && Char.myCharz().arr != null))
		{
			return;
		}
		Skill skill = null;
		for (int l = 0; l < GameScr.keySkill.Length; l++)
		{
			if (GameScr.keySkill[l] == null || GameScr.keySkill[l].paintCanNotUseSkill || GameScr.keySkill[l].template.id == 10 || GameScr.keySkill[l].template.id == 11 || GameScr.keySkill[l].template.id == 14 || GameScr.keySkill[l].template.id == 23 || GameScr.keySkill[l].template.id == 7 || Char.myCharz().skillInfoPaint() != null)
			{
				continue;
			}
			int num = ((GameScr.keySkill[l].template.manaUseType == 2) ? 1 : ((GameScr.keySkill[l].template.manaUseType == 1) ? (GameScr.keySkill[l].manaUse * Char.myCharz().cMPFull / 100) : GameScr.keySkill[l].manaUse));
			if (Char.myCharz().cMP >= num)
			{
				if (skill == null)
				{
					skill = GameScr.keySkill[l];
				}
				else if (skill.coolDown < GameScr.keySkill[l].coolDown)
				{
					skill = GameScr.keySkill[l];
				}
			}
		}
		if (skill != null)
		{
			GameScr.gI().doSelectSkill(skill, isShortcut: true);
			GameScr.gI().doDoubleClickToObj(Char.myCharz().mobFocus);
		}
	}

	public static void Update()
	{
		if (isAutoCo && mSystem.currentTimeMillis() - currAutoFlag >= 500)
		{
			if (!FlagInMap() && Char.myCharz().cFlag == 0)
			{
				Service.gI().getFlag(1, 8);
			}
			if (FlagInMap() && Char.myCharz().cFlag == 8)
			{
				Service.gI().getFlag(1, 0);
			}
			currAutoFlag = mSystem.currentTimeMillis();
		}
		if (isKOK && mSystem.currentTimeMillis() - currKOK >= 500)
		{
			currKOK = mSystem.currentTimeMillis();
			if (Char.myCharz().isCharge || Char.myCharz().isFlyAndCharge || Char.myCharz().isStandAndCharge || Char.myCharz().statusMe == 14 || Char.myCharz().cHP <= 0)
			{
				return;
			}
			Char.myCharz().cy--;
			Service.gI().charMove();
			Char.myCharz().cy++;
			Service.gI().charMove();
		}
		khoaViTri();
		if (!GameScr.canAutoPlay && isTDLT && GameCanvas.gameTick % 20 == 0)
		{
			TDLT();
		}
		AutoTTNL();
		AutoHoiSinh();
	}

	public static void AutoLogin()
	{
		dangLogin = true;
		Thread.Sleep(1000);
		GameCanvas.startOKDlg("Vui Lòng Đợi 25 Giây...");
		Thread.Sleep(23000);
		while (ServerListScreen.testConnect != 2)
		{
			GameCanvas.serverScreen.switchToMe();
			Thread.Sleep(1000);
		}
		if (GameCanvas.loginScr == null)
		{
			GameCanvas.loginScr = new LoginScr();
		}
		Thread.Sleep(1000);
		GameCanvas.loginScr.switchToMe();
		GameCanvas.loginScr.doLogin();
		dangLogin = false;
	}

	public static Skill GetSkillByIconID(int iconID)
	{
		for (int i = 0; i < GameScr.keySkill.Length; i++)
		{
			if (GameScr.keySkill[i] != null && GameScr.keySkill[i].template.iconId == iconID)
			{
				return GameScr.keySkill[i];
			}
		}
		return null;
	}

	public static void UseSkill(Skill sk)
	{
		if (Char.myCharz().myskill != sk)
		{
			GameScr.gI().doSelectSkill(sk, isShortcut: true);
			GameScr.gI().doSelectSkill(sk, isShortcut: true);
		}
		else
		{
			GameScr.gI().doSelectSkill(sk, isShortcut: true);
		}
	}

	public static int GetCoolDownSkill(Skill skill)
	{
		return (int)(skill.coolDown - mSystem.currentTimeMillis() + skill.lastTimeUseThisSkill);
	}

	public static void AutoTTNL()
	{
		if (!isAutoTTNL || mSystem.currentTimeMillis() - currAutoTTNL < 1000)
		{
			return;
		}
		currAutoTTNL = mSystem.currentTimeMillis();
		if (Char.myCharz().cgender != 2)
		{
			GameScr.info1.addInfo("Bạn Không Phải Người Xayda", 0);
			GameScr.info1.addInfo("Auto TTNL Đã tắt", 0);
			isAutoTTNL = false;
		}
		else if (!Char.myCharz().isStandAndCharge && !Char.myCharz().isCharge && !Char.myCharz().isFlyAndCharge && !Char.myCharz().stone && Char.myCharz().holdEffID == 0 && !Char.myCharz().blindEff && !Char.myCharz().sleepEff && Char.myCharz().cHP > 0 && Char.myCharz().statusMe != 14)
		{
			if ((Char.myCharz().cHP < Char.myCharz().cHPFull || Char.myCharz().cMP < Char.myCharz().cMPFull) && GetCoolDownSkill(GetSkillByIconID(720)) <= 0)
			{
				UseSkill(GetSkillByIconID(720));
			}
			else if (Char.myCharz().myskill != GetSkillByIconID(539))
			{
				GameScr.gI().doSelectSkill(GetSkillByIconID(539), isShortcut: true);
			}
		}
	}

	public static void AutoHoiSinh()
	{
		if (!isAutoHS || mSystem.currentTimeMillis() - currAutoHS < 1000)
		{
			return;
		}
		currAutoHS = mSystem.currentTimeMillis();
		if (Char.myCharz().cgender != 1)
		{
			GameScr.info1.addInfo("Bạn Không Phải Người Namek", 0);
			GameScr.info1.addInfo("Auto Hồi Sinh Namek Đã tắt", 0);
			isAutoHS = false;
		}
		else if (!Char.myCharz().isStandAndCharge && !Char.myCharz().isCharge && !Char.myCharz().isFlyAndCharge && !Char.myCharz().stone && Char.myCharz().holdEffID == 0 && !Char.myCharz().blindEff && !Char.myCharz().sleepEff && Char.myCharz().cHP > 0 && Char.myCharz().statusMe != 14)
		{
			if ((Char.myCharz().cHP < Char.myCharz().cHPFull || Char.myCharz().cMP < Char.myCharz().cMPFull) && GetCoolDownSkill(GetSkillByIconID(724)) <= 0)
			{
				UseSkill(GetSkillByIconID(724));
			}
			else if (Char.myCharz().myskill != GetSkillByIconID(539))
			{
				GameScr.gI().doSelectSkill(GetSkillByIconID(539), isShortcut: true);
			}
		}
	}

	public static void khoaViTri()
	{
		if (isKhoaViTri && mSystem.currentTimeMillis() - currKhoaViTri >= 600 && Char.myCharz().statusMe != 14 && Char.myCharz().cHP > 0)
		{
			currKhoaViTri = mSystem.currentTimeMillis();
			Char.myCharz().cx = ghimX;
			Char.myCharz().cy = ghimY;
			Service.gI().charMove();
		}
	}

	public static Char myPetInMap()
	{
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (@char != null && @char.charID == -Char.myCharz().charID)
			{
				return @char;
			}
		}
		return null;
	}

	public static bool FlagInMap()
	{
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (@char != null && @char.cFlag != 0 && @char.charID > 0 && ((Math.abs(@char.cx - Char.myCharz().cx) <= 500 && Math.abs(@char.cy - Char.myCharz().cy) <= 500) || (Math.abs(@char.cx - myPetInMap().cx) <= 500 && Math.abs(@char.cy - myPetInMap().cy) <= 500)))
			{
				return true;
			}
		}
		return false;
	}
}
