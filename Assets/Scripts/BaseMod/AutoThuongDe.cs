using System;
using System.Threading;


public class AutoThuongDe : IActionListener
{
	public static bool isauto;

	public static bool khongdu;

	private static int type;

	public int typenhando;

	private static AutoThuongDe instance;

	private int numTicket;

	private byte typePrice;

	private int Price;

	private bool isCrackBall;

	public bool nhandodangquay;

	public bool ispause;

	public static bool startmenu;

	public static AutoThuongDe gI()
	{
		if (instance == null)
		{
			return instance = new AutoThuongDe();
		}
		return instance;
	}

	public static void startMenu()
	{
		if (startmenu)
		{
			MyVector myVector = new MyVector();
			myVector.addElement(new Command("Mở thường", gI(), 1, 1));
			myVector.addElement(new Command("Mở đặc\nbiệt", gI(), 1, 2));
			myVector.addElement(new Command("Nhận đồ.", gI(), 2, null));
			GameCanvas.menu.startAt(myVector, 3);
		}
	}

	public void StartVongQuay(byte type, int price)
	{
		typePrice = type;
		Price = price;
		isCrackBall = true;
	}

	public void update()
	{
		if (isauto)
		{
			Service.gI().openMenu(19);
			Thread.Sleep(500);
			for (int i = 0; i < GameCanvas.menu.menuItems.size(); i++)
			{
				if (((Command)GameCanvas.menu.menuItems.elementAt(i)).caption.ToLower().Contains("quay"))
				{
					Service.gI().confirmMenu(19, (sbyte)i);
				}
			}
			Thread.Sleep(500);
			if (!isCrackBall)
			{
				for (int j = 0; j < GameCanvas.menu.menuItems.size(); j++)
				{
					Service.gI().confirmMenu(19, 0);
				}
			}
			Thread.Sleep(500);
			if (!isCrackBall)
			{
				Service.gI().confirmMenu(19, (sbyte)type);
			}
			Thread.Sleep(1000);
		}
		while (isauto)
		{
			try
			{
				checkNumTicket();
				int num = checkTien();
				if (num < Price)
				{
					isauto = false;
					GameScr.info1.addInfo("Không đủ tiền.", 0);
					GameScr.gI().switchToMe();
					break;
				}
				if (Price > 0)
				{
					int num2 = num / Price;
					num2 += numTicket;
					if (num2 > 7)
					{
						num2 = 7;
					}
					Service.gI().SendCrackBall(2, (byte)num2);
				}
				Thread.Sleep(2000);
				if (khongdu)
				{
					isauto = false;
					khongdu = false;
					GameScr.gI().switchToMe();
					GameScr.info1.addInfo("Xong.", 0);
					break;
				}
				continue;
			}
			catch (Exception)
			{
				continue;
			}
		}
		isCrackBall = false;
		GameScr.info1.addInfo("Xong.", 0);
	}

	public int checkTien()
	{
		return (int)((typePrice == 0) ? Char.myCharz().xu : Char.myCharz().checkLuong());
	}

	public static void infoMe(string s)
	{
		if ((s.Contains(mResources.not_enough_money_1) || s.ToLower().Contains("còn thiếu")) && !gI().nhandodangquay)
		{
			if (isauto)
			{
				khongdu = true;
			}
		}
		else if (gI().nhandodangquay)
		{
			if (GameScr.gI().isBagFull())
			{
				isauto = false;
				GameScr.gI().switchToMe();
				GameScr.info1.addInfo("Rương bạn đã đầy, kết thúc.", 0);
			}
			if (GameCanvas.keyAsciiPress == 113)
			{
				isauto = false;
				GameScr.gI().switchToMe();
				GameScr.info1.addInfo("Tắt Auto Quay", 0);
			}
			else
			{
				GameScr.gI().switchToMe();
				isauto = false;
				gI().typenhando = -2;
				gI().ispause = true;
				new Thread(gI().runnhando).Start();
			}
		}
		if (s.ToLower().Contains("rương phụ"))
		{
			isauto = false;
			GameScr.gI().switchToMe();
			GameScr.info1.addInfo("Xong.", 0);
		}
	}

	public void perform(int idAction, object p)
	{
		if (idAction == 1)
		{
			type = (int)p;
			isauto = true;
			new Thread(gI().update).Start();
		}
		if (idAction == 2)
		{
			MyVector myVector = new MyVector();
			myVector.addElement(new Command("Nhận vàng", gI(), 3, 9));
			myVector.addElement(new Command("Nhận bùa", gI(), 3, 13));
			myVector.addElement(new Command("Nhận đồ", gI(), 3, -1));
			myVector.addElement(new Command("Nhận cải trang", gI(), 3, 5));
			myVector.addElement(new Command("Nhận tất", gI(), 3, -2));
			GameCanvas.menu.startAt(myVector, 3);
		}
		if (idAction == 3)
		{
			typenhando = (int)p;
			new Thread(runnhando).Start();
		}
	}

	public void runnhando()
	{
		Service.gI().openMenu(19);
		Thread.Sleep(700);
		bool flag = false;
		int num = -1;
		int num2 = -1;
		for (int i = 0; i < GameCanvas.menu.menuItems.size(); i++)
		{
			Command obj = (Command)GameCanvas.menu.menuItems.elementAt(i);
			if (obj.caption.ToLower().Contains("quay"))
			{
				num2 = i;
			}
			if (obj.caption.ToLower().Contains("rương"))
			{
				num = i;
				break;
			}
		}
		if (num != -1)
		{
			Service.gI().confirmMenu(19, (sbyte)num);
			flag = true;
		}
		else
		{
			if (num2 != -1)
			{
				Service.gI().confirmMenu(19, (sbyte)num2);
				CloseMenu();
			}
			Thread.Sleep(200);
			for (int j = 0; j < GameCanvas.menu.menuItems.size(); j++)
			{
				if (((Command)GameCanvas.menu.menuItems.elementAt(j)).caption.ToLower().Contains("rương"))
				{
					Service.gI().confirmMenu(19, (sbyte)j);
					flag = true;
					break;
				}
			}
		}
		CloseMenu();
		Thread.Sleep(700);
		if (!flag)
		{
			GameScr.info1.addInfo("Không có đồ", 0);
			return;
		}
		Thread.Sleep(500);
		CloseMenu();
		if (typenhando == -1)
		{
			for (int k = 0; k < Char.myCharz().arrItemShop[0].Length; k++)
			{
				Item item = Char.myCharz().arrItemShop[0][k];
				if (GameScr.gI().isBagFull())
				{
					break;
				}
				if (item != null && (item.template.type == 0 || item.template.type == 1 || item.template.type == 2 || item.template.type == 3 || item.template.type == 4))
				{
					Service.gI().buyItem(0, k, 0);
					Thread.Sleep(500);
				}
			}
		}
		else if (typenhando != -2)
		{
			int num3 = 0;
			while (num3 < Char.myCharz().arrItemShop[0].Length)
			{
				Item item2 = Char.myCharz().arrItemShop[0][num3];
				if (GameScr.gI().isBagFull())
				{
					break;
				}
				if (item2 != null && item2.template.type == typenhando)
				{
					Service.gI().buyItem(0, num3, 0);
					Thread.Sleep(500);
				}
				else
				{
					num3++;
				}
			}
		}
		else
		{
			int num4 = 0;
			while (num4 < Char.myCharz().arrItemShop[0].Length)
			{
				Item item3 = Char.myCharz().arrItemShop[0][num4];
				if (GameScr.gI().isBagFull())
				{
					break;
				}
				if (item3 != null)
				{
					Service.gI().buyItem(0, num4, 0);
					Thread.Sleep(500);
				}
				else
				{
					num4++;
				}
			}
		}
		if (nhandodangquay && isauto)
		{
			isauto = true;
			new Thread(gI().update).Start();
		}
	}

	public void CloseMenu()
	{
		ChatPopup.currChatPopup = null;
		Effect2.vEffect2.removeAllElements();
		Effect2.vEffect2Outside.removeAllElements();
		InfoDlg.hide();
		GameCanvas.menu.doCloseMenu();
		GameCanvas.panel.cp = null;
	}

	private void checkNumTicket()
	{
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			Item item = Char.myCharz().arrItemBag[i];
			if (item != null && (item.template.id == 820 || item.template.id == 821))
			{
				numTicket = item.quantity;
				break;
			}
		}
	}

	private int checkTickket()
	{
		checkNumTicket();
		int num = 8;
		if (num > numTicket)
		{
			num = numTicket;
		}
		return num;
	}

	private int checkNum()
	{
		int num = 8;
		num -= checkTickket();
		if (num <= 0)
		{
			num = 0;
		}
		return num;
	}

	static AutoThuongDe()
	{
		isauto = false;
		khongdu = false;
		type = 1;
	}
}
