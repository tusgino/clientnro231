using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using UnityEngine;

public class MainMod : IActionListener, IChatable
{
	public static MainMod _Instance;

	public static int int_0;

	public static int int_1;

	public static int int_2;

	public static List<int> list_0;

	public static List<int> list_1;

	public static string string_2;

	public static int runSpeed;

	public static bool isAutoRevive;

	public static bool isLockFocus;

	public static int charIDLock;

	public static string[] inputLockFocusCharID;

	public static bool isConnectToAccountManager;

	public static int zoneIdNRD;

	public static int mapIdNRD;

	public static bool isOpenMenuNPC;

	public static bool isAutoEnterNRDMap;

	public static string[] nameMapsNRD;

	public static int int_4;

	public static bool bool_1;

	public static string[] inputHPPercentFusionDance;

	public static string[] inputHPFusionDance;

	public static int minumumHPPercentFusionDance;

	public static bool isPaintBackground;

	public static int minimumHPFusionDance;

	public static long long_0;

	public static List<int> listCharIDs;

	public static string[] inputCharID;

	public static bool isAutoLockControl;

	public static bool isAutoTeleport;

	public static long long_1;

	public static long long_2;

	public static bool isAutoAttackBoss;

	public static int HPLimit;

	public static string[] inputHPLimit;

	public static long long_3;

	public static bool isAutoAttackOtherChars;

	public static int limitHPChar;

	public static long long_4;

	public static string[] inputHPChar;

	public static bool isHuntingBoss;

	public static List<Boss> listBosses;

	public static Image logoServerListScreen;

	public static Image logoGameScreen;

	public static List<Image> listBackgroundImages;

	public static List<Color> listFlagColor;

	public static int widthRect;

	public static int heightRect;

	public static List<Char> listCharsInMap;

	public static bool isShowCharsInMap;

	public static string string_0;

	public static bool isReduceGraphics;

	public static bool isUsingSkill;

	public static long lastTimeConnected;

	public static bool isUsingCapsule;

	public static string string_1;

	public static int delay;

	public static Image image_0;

	public static int indexBackgroundImages;

	public static long lastTimeChangeBackground;

	public static string string_4;

	public static string string_3;

	public static int server;

	public static string APIKey;

	public static string APIServer;

	public static bool isSlovingCapcha;

	public static int int_3;

	public static long long_5;

	public static long long_6;

	public static bool bool_0;

	public static long long_7;

	public static bool isAutoT77;

	public static bool isAutoBomPicPoc;

	public static long timeCanAttack;

	public static bool isAttackBoss;

	public static int selectHP;

	public static bool map = true;

	public static bool site = true;

	public static bool name = false;

	public static bool time = true;

	public static bool PaintFlag;

	public static bool PaintEnemy;

	public static bool petw = false;

	public static bool charw = false;

	public static bool bosstrongkhu;

	public static bool isAutoLogin;

	public static bool islogo;

	public static bool ischunhay;

	public static bool linechar;

	public static MainMod getInstance()
	{
		if (_Instance == null)
		{
			_Instance = new MainMod();
		}
		return _Instance;
	}

	public static void Update()
	{
		if (isAttackBoss)
		{
			isMeCanAttack();
		}
		if ((!MobCapcha.isAttack || !MobCapcha.explode) && GameScr.gI().mobCapcha != null)
		{
			if (!isSlovingCapcha && GameCanvas.gameTick % 100 == 0)
			{
				isSlovingCapcha = true;
				new Thread(SolveCapcha).Start();
			}
			return;
		}
		if (isShowCharsInMap)
		{
			listCharsInMap.Clear();
			for (int i = 0; i < GameScr.vCharInMap.size(); i++)
			{
				Char @char = (Char)GameScr.vCharInMap.elementAt(i);
				if (@char.cName != null && @char.cName != "" && !@char.isPet && !@char.isMiniPet && !@char.cName.StartsWith("#") && !@char.cName.StartsWith("$") && @char.cName != "Trọng tài")
				{
					listCharsInMap.Add(@char);
				}
			}
		}
		if (isAutoEnterNRDMap)
		{
			EnterNRDMap();
		}
		if (isAutoRevive)
		{
			Revive();
		}
		if (isLockFocus)
		{
			FocusTo(charIDLock);
		}
		if (isConnectToAccountManager)
		{
			ConnectToAccountManager();
		}
		AutoItem.Update();
		AutoChat.Update();
		AutoPean.Update();
		AutoSkill.Update();
		AutoTrain.Update();
		AutoPick.Update();
		AutoMap.Update();
		AutoPoint.Update();
		Char.myCharz().cspeed = runSpeed;
	}

	public static void Paint(mGraphics g)
	{
		if (ischunhay)
		{
			if (GameCanvas.gameTick / (int)Time.timeScale % 10 != 0 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 1 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 2 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 3 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 4)
			{
				mFont.tahoma_7b_white.drawString(g, "Ngọc Rồng Muty".ToString(), GameCanvas.w / 2, 0, mFont.CENTER, mFont.tahoma_7);
			}
			else
			{
				mFont.tahoma_7b_red.drawString(g, "Ngọc Rồng Muty".ToString(), GameCanvas.w / 2, 0, mFont.CENTER, mFont.tahoma_7);
			}
			if (GameCanvas.gameTick / (int)Time.timeScale % 10 != 0 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 1 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 2 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 3 && GameCanvas.gameTick / (int)Time.timeScale % 10 != 4)
			{
				mFont.tahoma_7b_white.drawString(g, "Just For Fun".ToString(), GameCanvas.w / 2, 10, mFont.CENTER, mFont.tahoma_7);
			}
			else
			{
				mFont.tahoma_7b_red.drawString(g, "Just For Fun".ToString(), GameCanvas.w / 2, 10, mFont.CENTER, mFont.tahoma_7);
			}
		}
		if (bosstrongkhu)
		{
			mFont.tahoma_7b_yellow.drawString(g, "Boss trong khu:", GameCanvas.w / 2, 72, mFont.CENTER, mFont.tahoma_7);
			int num = 82;
			for (int i = 0; i < GameScr.vCharInMap.size(); i++)
			{
				Char @char = (Char)GameScr.vCharInMap.elementAt(i);
				if (@char != null && (@char.cTypePk == 5 || (@char.charID < 0 && @char.charID > -1000 && @char.charID != -114)) && !@char.isMiniPet)
				{
					mFont.tahoma_7b_red.drawString(g, i + " - " + (@char.isPet ? "$" : "") + (@char.isMiniPet ? "#" : "") + @char.cName + "[ " + NinjaUtil.getMoneys(@char.cHP).ToString() + " / " + NinjaUtil.getMoneys(@char.cHPFull).ToString() + " ] [ " + @char.getGender() + " ]", GameCanvas.w / 2, num, mFont.CENTER, mFont.tahoma_7);
					num += 10;
				}
			}
		}
		if (SpecialSkill.gI().isnoitai)
		{
			int num2 = Char.vItemTime.size() * 12 + 90;
			mFont.bigNumber_While.drawString(g, "Cần Mở: " + SpecialSkill.tennoitaicanmo, 150, num2 + 12, 0);
			mFont.bigNumber_While.drawString(g, "Nội tại hiện tại: " + Panel.specialInfo, 150, num2 + 24, 0);
			if (SpecialSkill.openMax)
			{
				mFont.bigNumber_While.drawString(g, "Max: " + SpecialSkill.max, 150, num2 + 36, 0);
			}
		}
		int num3 = 20;
		int yStart = 30;
		if (charw)
		{
			infoView(g, Char.myCharz(), num3, yStart);
			num3 += 120;
		}
		if (petw)
		{
			infoView(g, Char.myPetz(), num3, yStart);
		}
		
		GUIStyle gUIStyle = new GUIStyle(GUI.skin.label);
		gUIStyle.normal.textColor = Color.green;
		gUIStyle.fontSize = 17;
		g.drawString(NinjaUtil.getMoneys(Char.myCharz().cHP), 85, 4, gUIStyle);
		gUIStyle.fontSize = 10;
		g.drawString(NinjaUtil.getMoneys(Char.myCharz().cMP), 85, 19, gUIStyle);
		paintListBosses(g);

		FPSMS.Update(g);


		if (site)
		{
			mFont.tahoma_7_white.drawString(g, "X[" + Char.myCharz().cx + "] - Y[" + Char.myCharz().cy + "]", 20, 150, mFont.LEFT, mFont.tahoma_7b_dark);
		}
		if (name)
		{
			mFont.tahoma_7_white.drawString(g, "Name: " + Char.myCharz().cName, 20, 160, mFont.LEFT, mFont.tahoma_7b_dark);
		}
		if (map)
		{
			mFont.tahoma_7_white.drawString(g, "Map: " + TileMap.mapName + " [ID:" + TileMap.mapID + "] - [Khu: " + TileMap.zoneID + "]", 20, 140, mFont.LEFT, mFont.tahoma_7b_dark);
		}
		if (time)
		{
			mFont.tahoma_7_white.drawString(g, string.Concat(new string[2]
			{
				"Time: ",
				DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy")
			}), 20, 130, mFont.LEFT, mFont.tahoma_7b_dark);
		}
		int num4 = GameCanvas.h - 200;
		if (isConnectToAccountManager)
		{
			mFont.tahoma_7.drawString(g, "Đã kết nối!", 25, num4, 0);
			num4 += 10;
		}
		if (AutoSkill.isAutoSendAttack)
		{
			mFont.tahoma_7.drawString(g, "Tự đánh: on", 25, num4, 0);
			num4 += 10;
		}
		if (isAutoRevive)
		{
			mFont.tahoma_7.drawString(g, "Hồi sinh: on", 25, num4, 0);
			num4 += 10;
		}
		if (AutoPick.isAutoPick)
		{
			mFont.tahoma_7.drawString(g, "Auto nhặt: on", 25, num4, 0);
			num4 += 10;
		}
		if (isLockFocus)
		{
			mFont.tahoma_7.drawString(g, "Khóa: " + charIDLock, 25, num4, 0);
			num4 += 10;
		}
		if (isAutoEnterNRDMap)
		{
			mFont.tahoma_7.drawString(g, "Đang auto nrd: " + mapIdNRD + "sk" + zoneIdNRD, 25, num4, 0);
			num4 += 10;
		}
	}

	public static void paintListCharsInMap(mGraphics g)
	{
		int num = 175;
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			string text = "";
			if (@char.cTypePk == 5)
			{
				text = "BOSS";
			}
			else if (@char.nClass.classId == 0)
			{
				text = "TD";
			}
			else if (@char.nClass.classId == 1)
			{
				text = "NM";
			}
			else if (@char.nClass.classId == 2)
			{
				text = "XD";
			}
			if (PaintEnemy)
			{
				if (@char != null && !@char.cName.Contains("Đệ") && @char.cTypePk != 0)
				{
					g.fillRect(360, GameCanvas.h - num, 150, 12, 54, 8);
					((@char == Char.myCharz().charFocus) ? mFont.tahoma_7b_red : mFont.tahoma_7b_white).drawString(g, string.Concat(new object[1] { @char.cName + " [" + NinjaUtil.getMoneys(@char.cHP) + " - " + text + "]" }), 370, GameCanvas.h - num, mFont.LEFT, mFont.tahoma_7b_dark);
					if (PaintFlag && @char.cFlag != 0)
					{
						SmallImage.drawSmallImage(g, 2323 + @char.cFlag, 363, GameCanvas.h - num + 10, -1, 33);
					}
					num -= 10;
				}
			}
			else if (@char != null && !@char.cName.Contains("Đệ"))
			{
				g.fillRect(360, GameCanvas.h - num, 150, 12, 54, 8);
				((@char == Char.myCharz().charFocus) ? mFont.tahoma_7b_red : mFont.tahoma_7b_white).drawString(g, string.Concat(new object[1] { @char.cName + " [" + NinjaUtil.getMoneys(@char.cHP) + " - " + text + "]" }), 370, GameCanvas.h - num, mFont.LEFT, mFont.tahoma_7b_dark);
				if (PaintFlag && @char.cFlag != 0)
				{
					SmallImage.drawSmallImage(g, 2323 + @char.cFlag, 363, GameCanvas.h - num + 10, -1, 33);
				}
				num -= 10;
			}
		}
	}

	public static void paintCharInfo(mGraphics g, Char ch)
	{
		mFont.tahoma_7b_red.drawString(g, ch.cName + " [" + NinjaUtil.getMoneys(ch.cHP) + "/" + NinjaUtil.getMoneys(ch.cHPFull) + "]", GameCanvas.w / 2, 62, 2);
		int num = 72;
		int num2 = 10;
		if (ch.isNRD)
		{
			mFont.tahoma_7b_red.drawString(g, "Còn: " + ch.timeNRD + " giây", GameCanvas.w / 2, num, 2);
			num += num2;
		}
		if (ch.isFreez)
		{
			mFont.tahoma_7b_red.drawString(g, "Bị TDHS: " + ch.freezSeconds + " giây", GameCanvas.w / 2, num, 2);
			num += num2;
		}
	}

	public static void smethod_15(mGraphics g, int x, int y)
	{
	}

	public static void paintListBosses(mGraphics g)
	{
		if (isHuntingBoss && !isMeInNRDMap())
		{
			int num = 42;
			for (int i = 0; i < listBosses.Count; i++)
			{
				g.setColor(2721889, 0.5f);
				g.fillRect(GameCanvas.w - 23, num + 2, 21, 9);
				listBosses[i].Paint(g, GameCanvas.w - 2, num, mFont.RIGHT);
				num += 10;
			}
		}
	}

	public void onChatFromMe(string text, string to)
	{
		if (ChatTextField.gI().tfChat.getText() != null && !ChatTextField.gI().tfChat.getText().Equals(string.Empty) && !text.Equals(string.Empty) && text != null)
		{
			if (ChatTextField.gI().strChat.Equals(inputLockFocusCharID[0]))
			{
				try
				{
					int num = (charIDLock = int.Parse(ChatTextField.gI().tfChat.getText()));
					isLockFocus = true;
					GameScr.info1.addInfo("Đã Thêm: " + num, 0);
				}
				catch
				{
					GameScr.info1.addInfo("CharID Không Hợp Lệ, Vui Lòng Nhập Lại", 0);
				}
				ResetChatTextField();
			}
			else if (ChatTextField.gI().strChat.Equals(inputHPFusionDance[0]))
			{
				try
				{
					int num2 = (minimumHPFusionDance = int.Parse(ChatTextField.gI().tfChat.getText()));
					GameScr.info1.addInfo("Hợp Thể Khi HP Dưới: " + Res.formatNumber2(num2), 0);
				}
				catch
				{
					GameScr.info1.addInfo("HP Không Hợp Lệ, Vui Lòng Nhập Lại!", 0);
				}
				ResetChatTextField();
			}
			else if (ChatTextField.gI().strChat.Equals(inputCharID[0]))
			{
				try
				{
					int item = int.Parse(ChatTextField.gI().tfChat.getText());
					listCharIDs.Add(item);
					GameScr.info1.addInfo("Đã Thêm: " + item, 0);
				}
				catch
				{
					GameScr.info1.addInfo("CharID Không Hợp Lệ, Vui Lòng Nhập Lại!", 0);
				}
				ResetChatTextField();
			}
			else if (ChatTextField.gI().strChat.Equals(inputHPLimit[0]))
			{
				try
				{
					int num3 = (HPLimit = int.Parse(ChatTextField.gI().tfChat.getText()));
					GameScr.info1.addInfo("Limit: " + NinjaUtil.getMoneys(num3) + " HP", 0);
				}
				catch
				{
					GameScr.info1.addInfo("HP Không Hợp Lệ, Vui Lòng Nhập Lại!", 0);
				}
				ResetChatTextField();
			}
			else if (ChatTextField.gI().strChat.Equals(inputHPChar[0]))
			{
				try
				{
					int num4 = (limitHPChar = int.Parse(ChatTextField.gI().tfChat.getText()));
					GameScr.info1.addInfo("Limit: " + NinjaUtil.getMoneys(num4) + " HP", 0);
				}
				catch
				{
					GameScr.info1.addInfo("HP Không Hợp Lệ, Vui Lòng Nhập Lại!", 0);
				}
				ResetChatTextField();
			}
			else
			{
				if (!ChatTextField.gI().strChat.Equals(inputHPPercentFusionDance[0]))
				{
					return;
				}
				try
				{
					int num5 = int.Parse(ChatTextField.gI().tfChat.getText());
					if (num5 > 99)
					{
						num5 = 99;
					}
					minumumHPPercentFusionDance = num5;
					GameScr.info1.addInfo("Hợp Thể Khi HP Dưới: " + num5 + "%", 0);
				}
				catch
				{
					GameScr.info1.addInfo("%HP Không Hợp Lệ, Vui Lòng Nhập Lại!", 0);
				}
				ResetChatTextField();
			}
		}
		else
		{
			ChatTextField.gI().isShow = false;
			ResetChatTextField();
		}
	}

	public void onCancelChat()
	{
	}

	public void perform(int idAction, object p)
	{
		switch (idAction)
		{
		case 1:
			AutoMap.ShowMenu();
			ChatPopup.addChatPopup("|2|Đây Là Menu Auto Map", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 7992
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 2:
			AutoSkill.ShowMenu();
			ChatPopup.addChatPopup("|1|Đây Là Menu Auto Skill!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 8057
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 3:
			AutoPean.ShowMenu();
			ChatPopup.addChatPopup("|1|Hô Hô Hô, Chào Ngươi: " + Char.myCharz().cName + "\n|8|Ngươi Muốn Dùng Gì Nào?", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 5825
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 4:
			AutoPick.ShowMenu();
			ChatPopup.addChatPopup("|1|Ồ, Là Một Anh Chàng Đẹp Trai \n|7|Anh Cần Gì?", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 3049
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 5:
			AutoTrain.ShowMenu();
			ChatPopup.addChatPopup("|1|Mùa Đông Không Có Người Yêu Ôm À?\n Bảo Sao Chỉ Có " + Char.myCharz().cPower + " Sức Mạnh\n|0|Chọn Menu Mà Up Đi Người Anh Em, HAHA!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 2793
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 6:
			AutoChat.ShowMenu();
			ChatPopup.addChatPopup("|1|Cậu Hãy Chọn Option Auto Chat!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 8025
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 7:
			AutoPoint.ShowMenu();
			ChatPopup.addChatPopup("|1|Cậu Hãy Chọn Option Auto Point!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 8057
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 8:
			ShowMenuMore();
			ChatPopup.addChatPopup("|0|Show More!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 8057
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 9:
			if (minumumHPPercentFusionDance > 0)
			{
				minumumHPPercentFusionDance = 0;
				GameScr.info1.addInfo("Hợp thể khi HP dưới: 0% HP", 0);
			}
			else
			{
				ChatTextField.gI().strChat = inputHPPercentFusionDance[0];
				ChatTextField.gI().tfChat.name = inputHPPercentFusionDance[1];
				ChatTextField.gI().startChat2(getInstance(), string.Empty);
			}
			break;
		case 10:
			if (minimumHPFusionDance > 0)
			{
				minimumHPFusionDance = 0;
				GameScr.info1.addInfo("Hợp thể khi HP dưới: 0", 0);
			}
			else
			{
				ChatTextField.gI().strChat = inputHPFusionDance[0];
				ChatTextField.gI().tfChat.name = inputHPFusionDance[1];
				ChatTextField.gI().startChat2(getInstance(), string.Empty);
			}
			break;
		case 11:
			smethod_2();
			break;
		case 12:
			isAutoLockControl = !isAutoLockControl;
			GameScr.info1.addInfo("Auto Khống Chế\n" + (isAutoLockControl ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			break;
		case 13:
			isAutoTeleport = !isAutoTeleport;
			GameScr.info1.addInfo("Auto Teleport\n" + (isAutoTeleport ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			break;
		case 14:
			smethod_3();
			break;
		case 15:
			ChatTextField.gI().strChat = inputCharID[0];
			ChatTextField.gI().tfChat.name = inputCharID[1];
			ChatTextField.gI().startChat2(getInstance(), string.Empty);
			break;
		case 16:
		{
			int num = (int)p;
			if (num != 0)
			{
				listCharIDs.Add(num);
				GameScr.info1.addInfo("Đã Thêm: " + num, 0);
			}
			break;
		}
		case 17:
		{
			int num2 = (int)p;
			if (num2 != 0)
			{
				listCharIDs.Remove(num2);
				GameScr.info1.addInfo("Đã Xóa: " + num2, 0);
			}
			break;
		}
		case 18:
			ShowMenuBoss();
			break;
		case 19:
			isAttackBoss = !isAttackBoss;
			GameScr.info1.addInfo("Tấn Công Boss\n" + (isAttackBoss ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			break;
		case 20:
			ChatTextField.gI().strChat = inputHPLimit[0];
			ChatTextField.gI().tfChat.name = inputHPLimit[1];
			ChatTextField.gI().startChat2(getInstance(), string.Empty);
			break;
		case 21:
			smethod_1();
			break;
		case 22:
			isAutoAttackOtherChars = !isAutoAttackOtherChars;
			GameScr.info1.addInfo("Tàn Sát Người\n" + (isAutoAttackOtherChars ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			break;
		case 23:
			ChatTextField.gI().strChat = inputHPChar[0];
			ChatTextField.gI().tfChat.name = inputHPChar[1];
			ChatTextField.gI().startChat2(getInstance(), string.Empty);
			break;
		case 24:
			GameScr.info1.addInfo("Tính năng chưa hoàn thiện, vui lòng chờ bản update!", 0);
			break;
		case 25:
			GameScr.info1.addInfo("Tính năng chưa hoàn thiện, vui lòng chờ bản update!", 0);
			break;
		case 26:
			GameScr.info1.addInfo("Tính năng chưa hoàn thiện, vui lòng chờ bản update!", 0);
			break;
		case 27:
			GameScr.info1.addInfo("Tính năng chưa hoàn thiện, vui lòng chờ bản update!", 0);
			break;
		case 28:
			isPaintBackground = !isPaintBackground;
			Rms.saveRMSInt("isPaintBgr", isPaintBackground ? 1 : 0);
			break;
		case 29:
			isHuntingBoss = !isHuntingBoss;
			Rms.saveRMSInt("sanboss", isHuntingBoss ? 1 : 0);
			break;
		case 30:
			isShowCharsInMap = !isShowCharsInMap;
			Rms.saveRMSInt("showchar", isShowCharsInMap ? 1 : 0);
			break;
		case 31:
			ShowMenuHienThi();
			break;
		case 32:
			isAutoT77 = !isAutoT77;
			GameScr.info1.addInfo("Auto T77\n" + (isAutoT77 ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 33:
			isAutoBomPicPoc = !isAutoBomPicPoc;
			GameScr.info1.addInfo("Auto Bom\nPic Poc" + (isAutoBomPicPoc ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 34:
			petw = !petw;
			GameScr.info1.addInfo("Pet View " + (petw ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 35:
			map = !map;
			GameScr.info1.addInfo("View Map" + (map ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 36:
			time = !time;
			GameScr.info1.addInfo("View Time" + (time ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 37:
			site = !site;
			GameScr.info1.addInfo("View Site" + (site ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 38:
			name = !name;
			GameScr.info1.addInfo("View Name" + (name ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 39:
			ShowMenuThongTin();
			break;
		case 40:
			charw = !charw;
			GameScr.info1.addInfo("Char View " + (charw ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 41:
			petw = !petw;
			GameScr.info1.addInfo("Pet View " + (petw ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			break;
		case 42:
			ShowMenuChucNang();
			ChatPopup.addChatPopup("|1|ArcMenu ShowCase!", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 8057
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 43:
			ShowMenuAutoItem();
			break;
		case 44:
			ShowMenuAutoScreen();
			break;
		case 45:
			ShowMenuAutoMore();
			break;
		case 46:
			ShowMenuAutoUp();
			break;
		case 47:
			AutoSuDungItem.cn = !AutoSuDungItem.cn;
			new Thread(AutoSuDungItem.autocuongno).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Cuồng Nộ: " + (AutoSuDungItem.cn ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 48:
			AutoSuDungItem.bh = !AutoSuDungItem.bh;
			new Thread(AutoSuDungItem.autobohuyet).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Bổ Huyết: " + (AutoSuDungItem.bh ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 49:
			AutoSuDungItem.bk = !AutoSuDungItem.bk;
			new Thread(AutoSuDungItem.autobokhi).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Bổ Khí: " + (AutoSuDungItem.bk ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 50:
			AutoSuDungItem.gx = !AutoSuDungItem.gx;
			new Thread(AutoSuDungItem.autogiapxen).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Giáp Xên: " + (AutoSuDungItem.gx ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 51:
			AutoSuDungItem.ad = !AutoSuDungItem.ad;
			new Thread(AutoSuDungItem.autoandanh).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Ẩn Danh: " + (AutoSuDungItem.ad ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 52:
			AutoSuDungItem.md = !AutoSuDungItem.md;
			new Thread(AutoSuDungItem.automaydo).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Máy Dò: " + (AutoSuDungItem.ad ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 53:
			AutoSuDungItem.cskb = !AutoSuDungItem.cskb;
			new Thread(AutoSuDungItem.autocskb).Start();
			GameScr.info1.addInfo("Auto CSKB: " + (AutoSuDungItem.cskb ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 54:
			UpgradeItem.isDapDo = !UpgradeItem.isDapDo;
			new Thread(UpgradeItem.AutoDapDo).Start();
			GameScr.info1.addInfo("Auto Đập Đồ: " + (UpgradeItem.isDapDo ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 55:
			if (TileMap.mapID == 45)
			{
				AutoThuongDe.startmenu = !AutoThuongDe.startmenu;
				new Thread(AutoThuongDe.startMenu).Start();
				GameScr.info1.addInfo("Auto VQTD: " + (AutoThuongDe.startmenu ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
				break;
			}
			ChatPopup.addBigMessage("|0|Bạn Không Ở Thần Điện!\n|1|Bạn Có Muốn Đến Thần Điện Để Sử Dụng Auto Không?", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 7992
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("Đến!", ChatPopup.serverChatPopUp, 9991, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("Không", ChatPopup.serverChatPopUp, 9992, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			break;
		case 56:
			ShowMenuItemCap2();
			break;
		case 57:
			AutoSuDungItem.cnc2 = !AutoSuDungItem.cnc2;
			new Thread(AutoSuDungItem.autocuongnoc2).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Cuồng Nộ Siêu Cấp: " + (AutoSuDungItem.cnc2 ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 58:
			AutoSuDungItem.bhc2 = !AutoSuDungItem.bhc2;
			new Thread(AutoSuDungItem.autobohuyetc2).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Bổ Huyết Siêu Cấp: " + (AutoSuDungItem.bhc2 ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 59:
			AutoSuDungItem.bkc2 = !AutoSuDungItem.bkc2;
			new Thread(AutoSuDungItem.autobokhic2).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Bổ Khí Siêu Cấp: " + (AutoSuDungItem.bkc2 ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 60:
			AutoSuDungItem.gxc2 = !AutoSuDungItem.gxc2;
			new Thread(AutoSuDungItem.autogiapxenc2).Start();
			GameScr.info1.addInfo("Auto Sử Dụng Giáp Xên Siêu Cấp: " + (AutoSuDungItem.gxc2 ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 61:
			AutoYardrat.isautobikiep = !AutoYardrat.isautobikiep;
			if (AutoYardrat.isautobikiep)
			{
				AutoYardrat.khienIndex = findSkillIndex(19);
				AutoYardrat.ttnlIndex = findSkillIndex(8);
				new Thread(AutoYardrat.update).Start();
			}
			GameScr.info1.addInfo("Auto Up Yardrat: " + (AutoYardrat.isautobikiep ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 62:
			Tugino.isTDLT = !Tugino.isTDLT;
			GameScr.info1.addInfo("TDLT: " + (Tugino.isTDLT ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 63:
			AutoSuDungItem.isAutoBT = !AutoSuDungItem.isAutoBT;
			GameScr.info1.addInfo("Auto bông tai: " + (AutoSuDungItem.isAutoBT ? "Bật" : "Tắt"), 0);
			break;
		case 64:
			bosstrongkhu = !bosstrongkhu;
			GameScr.info1.addInfo("Boss Trong Khu: " + (bosstrongkhu ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 65:
			isAutoLogin = !isAutoLogin;
			GameScr.info1.addInfo("Auto Login: " + (isAutoLogin ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 66:
			ischunhay = !ischunhay;
			islogo = !islogo;
			GameScr.info1.addInfo("Ẩn Logo: " + (islogo ? "[STATUS:OFF]" : "[STATUS:ON]"), 0);
			break;
		case 67:
			Tugino.isAutoTTNL = !Tugino.isAutoTTNL;
			GameScr.info1.addInfo("Auto Tái Tạo Năng Lượng: " + (Tugino.isAutoTTNL ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 68:
			Tugino.isAutoHS = !Tugino.isAutoHS;
			GameScr.info1.addInfo("Auto Hồi Sinh: " + (Tugino.isAutoHS ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 69:
			Tugino.ghimX = Char.myCharz().cx;
			Tugino.ghimY = Char.myCharz().cy;
			Tugino.isKhoaViTri = !Tugino.isKhoaViTri;
			GameScr.info1.addInfo("Khóa Vị Trí: " + (Tugino.isKhoaViTri ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 70:
			Tugino.isKhoaKhu = !Tugino.isKhoaKhu;
			GameScr.info1.addInfo("Khóa Khu: " + (Tugino.isKhoaKhu ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 71:
			Tugino.isKOK = !Tugino.isKOK;
			GameScr.info1.addInfo("Auto Up Kaioken: " + (Tugino.isKOK ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 72:
			Tugino.isAutoCo = !Tugino.isAutoCo;
			GameScr.info1.addInfo("Auto Flag: " + (Tugino.isAutoCo ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 73:
			GameScr.xoamap = !GameScr.xoamap;
			GameScr.info1.addInfo("Delete Map: " + (GameScr.xoamap ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 74:
			GameScr.hideInterface = !GameScr.hideInterface;
			GameScr.info1.addInfo("Delete All: " + (GameScr.hideInterface ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 75:
			if (GameScr.isRongThanXuatHien == false)
			{
				GameCanvas.troiden = !GameCanvas.troiden;
				GameCanvas.troixanh = true;
				GameScr.info1.addInfo("Dark BG Is On! Please,Turn Off Dark BG", 0);
			}
			GameCanvas.troixanh = false;
			isPaintBackground = false;
			GameCanvas.troiden = !GameCanvas.troiden;
			GameScr.info1.addInfo("Black Background: " + (GameCanvas.troiden ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 76:
			GameScr.isRongThanXuatHien = !GameScr.isRongThanXuatHien;
			GameCanvas.troiden = false;
			GameCanvas.troixanh = false;
			isPaintBackground = false;
			GameScr.info1.addInfo("Dark Background: " + (GameScr.isRongThanXuatHien ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 77:
			linechar = !linechar;
			GameScr.info1.addInfo("Line Char: " + (linechar ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			break;
		case 100:
			Application.OpenURL("http://acc957.com/");
			break;
		case 101:
			Application.OpenURL("http://vangngoc957.com/");
			break;
		case 102:
			Application.OpenURL("https://www.youtube.com/channel/UCkE_Mbny4y1BREb2E-sSZvQ");
			break;
		case 103:
			Application.OpenURL("https://www.facebook.com/octiiu957.official");
			break;
		case 104:
			Application.OpenURL("https://www.facebook.com/groups/TEAM957/");
			break;
		case 105:
			Application.OpenURL("https://www.facebook.com/pham.dung177/");
			break;
		case 106:
			Application.OpenURL("https://www.youtube.com/channel/UCx2ehE3tT4bRGpFXb1IsKhw");
			break;
		case 107:
			Application.OpenURL("https://dungpham.com.vn/");
			break;
		case 108:
			GameCanvas.startOKDlg("Để nâng cấp phiên bản cần liên hệ Dũng Phạm\nTrong phiên bản mới sẽ có thêm nhiều tính năng như QLTK, điều khiển tab (bom cả dàn ac cùng lúc,... ), hiển thị đầy đủ thông tin người ôm ngọc rồng đen (time khiên, khỉ, thôi miên,... ), và nhiều tính năng khác, hỗ trợ tối đa ngọc rồng đen, pk và săn boss\nPhiên bản nâng cấp sẽ được hỗ trợ update fix lỗi free liên tục trong quá trình sử dụng\nGía: 300k/1key/sv - HSD: vĩnh viễn");
			break;
		}
	}

	public static bool UpdateKey(int unused)
	{
		if (GameCanvas.keyAsciiPress == Hotkeys.A)
		{
			AutoSkill.isAutoSendAttack = !AutoSkill.isAutoSendAttack;
			GameScr.info1.addInfo("Tự Đánh\n" + (AutoSkill.isAutoSendAttack ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.B)
		{
			Service.gI().friend(0, -1);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.C)
		{
			for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
			{
				Item item = Char.myCharz().arrItemBag[i];
				if (item != null && (item.template.id == 194 || item.template.id == 193))
				{
					Service.gI().useItem(0, 1, (sbyte)item.indexUI, -1);
					break;
				}
			}
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.D)
		{
			AutoSkill.FreezeSelectedSkill();
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.E)
		{
			isAutoRevive = !isAutoRevive;
			GameScr.info1.addInfo("Auto Hồi Sinh\n" + (isAutoRevive ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.F)
		{
			bool isNhapThe = Char.myCharz().isNhapThe;
			if (!UseItem(2103))
            {
				if (!UseItem(2075))
				{
					if (!UseItem(2074))
					{
						if (!UseItem(921))
						{
							if (!UseItem(454))
							{
								GameScr.info1.addInfo("Không có bông tai!", 0);
							}
						}
					}
				}
			}
			if (isNhapThe)
			{
				Service.gI().petStatus(3);
			}
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.G)
		{
			if (Char.myCharz().charFocus == null)
			{
				GameScr.info1.addInfo("Vui Lòng Chọn Mục Tiêu!", 0);
			}
			else
			{
				Service.gI().giaodich(0, Char.myCharz().charFocus.charID, -1, -1);
				GameScr.info1.addInfo("Đã Gửi Lời Mời Giao Dịch Đến: " + Char.myCharz().charFocus.cName, 0);
			}
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.I)
		{
			isLockFocus = !isLockFocus;
			if (!isLockFocus)
			{
				GameScr.info1.addInfo("Khoá Mục Tiêu\n[STATUS: OFF]", 0);
			}
			else if (Char.myCharz().charFocus == null)
			{
				GameScr.info1.addInfo("Vui Lòng Chọn Mục Tiêu!", 0);
			}
			else
			{
				charIDLock = Char.myCharz().charFocus.charID;
				GameScr.info1.addInfo("Đã Khóa: " + Char.myCharz().charFocus.cName, 0);
			}
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.J)
		{
			AutoMap.LoadMapLeft();
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.K)
		{
			AutoMap.LoadMapCenter();
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.L)
		{
			AutoMap.LoadMapRight();
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.M)
		{
			Service.gI().openUIZone();
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.N)
		{
			if (isMeInNRDMap())
			{
				AutoPick.isAutoPick = !AutoPick.isAutoPick;
				GameScr.info1.addInfo("Auto Nhặt\n" + (AutoPick.isAutoPick ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			}
			else
			{
				AutoPick.ShowMenu();
			}
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.O)
		{
			isAutoEnterNRDMap = !isAutoEnterNRDMap;
			isOpenMenuNPC = true;
			GameScr.info1.addInfo("Auto Vào NRD\n" + (isAutoEnterNRDMap ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.P)
		{
			isConnectToAccountManager = !isConnectToAccountManager;
			GameScr.info1.addInfo("Kết Nối\n" + (isConnectToAccountManager ? "[STATUS: ON]" : "[STATUS: OFF]"), 0);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.T)
		{
			UseItem(521);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.X)
		{
			ShowMenu();
			ChatPopup.addChatPopup("|1|Ngọc Rồng Muty Xin Chào Bạn: " + Char.myCharz().cName + "\nĐệ Tử: " + Char.myPetz().cName + "\nSức Mạnh: " + Char.myCharz().cPower + " - Pet: " + Char.myPetz().cPower + "\nTiềm Năng: " + Char.myCharz().cTiemNang + " - Pet: " + Char.myPetz().cTiemNang + "\nHP: " + Char.myCharz().cHP + " - Pet: " + Char.myPetz().cHP + "\nKI: " + Char.myCharz().cMP + " - Pet: " + Char.myPetz().cMP + "\nSức Đánh: " + Char.myCharz().cDamFull + " - Pet: " + Char.myPetz().cDamFull + "\n Sức Đánh Gốc - Pet: " + Char.myPetz().cDamGoc + "\nGiáp: " + Char.myCharz().cDefull + " - Pet: " + Char.myPetz().cDefull + "\nGiáp Gốc - Pet: " + Char.myPetz().cDefGoc + "\nChí Mạng: " + Char.myCharz().cCriticalFull + " - Pet: " + Char.myPetz().cCriticalFull + "\nChí Mạng Gốc - Pet: " + Char.myPetz().cCriticalGoc + "\n|0|Đây Là Menu Mod Game", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 7992
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("", ChatPopup.serverChatPopUp, 1000, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("", ChatPopup.serverChatPopUp, 1001, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.Z)
		{
			TextEditor textEditor = new TextEditor();
			textEditor.Paste();
			Service.gI().chat(textEditor.text);
			return true;
		}
		if (GameCanvas.keyAsciiPress == Hotkeys.S)
		{
			for (int j = 0; j < GameScr.vCharInMap.size(); j++)
			{
				Char @char = (Char)GameScr.vCharInMap.elementAt(j);
				if (!@char.cName.Equals("") && isBoss(@char) && (Char.myCharz().charFocus == null || (Char.myCharz().charFocus != null && Char.myCharz().charFocus.cName != @char.cName)))
				{
					Char.myCharz().charFocus = @char;
					break;
				}
			}
			return true;
		}
		return false;
	}

	public static void ShowMenu()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Chức Năng", getInstance(), 42, null));
		myVector.addElement(new Command("Auto Map", getInstance(), 1, null));
		myVector.addElement(new Command("Auto Skill", getInstance(), 2, null));
		myVector.addElement(new Command("Auto Pean", getInstance(), 3, null));
		myVector.addElement(new Command("Auto Pick", getInstance(), 4, null));
		myVector.addElement(new Command("Auto Train", getInstance(), 5, null));
		myVector.addElement(new Command("Auto Chat", getInstance(), 6, null));
		myVector.addElement(new Command("Auto Point", getInstance(), 7, null));
		myVector.addElement(new Command("More", getInstance(), 8, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuMore()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Background\n" + (isPaintBackground ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 28, null));
		myVector.addElement(new Command("Thông Báo\nBoss\n" + (isHuntingBoss ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 29, null));
		myVector.addElement(new Command("Danh Sách\nNgười Trong Map\n" + (isShowCharsInMap ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 30, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void smethod_0()
	{
	}

	public static void smethod_1()
	{
	}

	public static void smethod_2()
	{
	}

	public static void smethod_3()
	{
	}

	public static void ResetChatTextField()
	{
		ChatTextField.gI().strChat = "Chat";
		ChatTextField.gI().tfChat.name = "chat";
		ChatTextField.gI().isShow = false;
	}

	public static bool UseItem(int templateId)
	{
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			Item item = Char.myCharz().arrItemBag[i];
			if (item != null && item.template.id == templateId)
			{
				Service.gI().useItem(0, 1, (sbyte)item.indexUI, -1);
				return true;
			}
		}
		return false;
	}

	public static void TeleportTo(int x, int y)
	{
		Char.myCharz().cx = x;
		Char.myCharz().cy = y;
		Service.gI().charMove();
		Char.myCharz().cx = x;
		Char.myCharz().cy = y + 1;
		Service.gI().charMove();
		Char.myCharz().cx = x;
		Char.myCharz().cy = y;
		Service.gI().charMove();
	}

	public static int GetYGround(int x)
	{
		int num = 50;
		int num2 = 0;
		while (num2 < 30)
		{
			num2++;
			num += 24;
			if (TileMap.tileTypeAt(x, num, 2))
			{
				if (num % 24 != 0)
				{
					num -= num % 24;
				}
				break;
			}
		}
		return num;
	}

	static MainMod()
	{
		islogo = true;
		listFlagColor = new List<Color>();
		listCharsInMap = new List<Char>();
		isShowCharsInMap = true;
		string_0 = "https://www.facebook.com/nguyen.dinh.tu.20.07/";
		isHuntingBoss = true;
		listBosses = new List<Boss>();
		listBackgroundImages = new List<Image>();
		limitHPChar = -1;
		inputHPChar = new string[2] { "Nhập HP Char:", "HP" };
		inputHPLimit = new string[2] { "Nhập HP:", "HP" };
		listCharIDs = new List<int>();
		inputCharID = new string[2] { "Nhập charID:", "charID" };
		inputHPPercentFusionDance = new string[2] { "Nhập %HP ", "%HP" };
		inputHPFusionDance = new string[2] { "Nhập HP", "HP" };
		nameMapsNRD = new string[7] { "Hành tinh M-2", "Hành tinh Polaris", "Hành tinh Cretaceous", "Hành tinh Monmaasu", "Hành tinh Rudeeze", "Hành tinh Gelbo", "Hành tinh Tigere" };
		inputLockFocusCharID = new string[2] { "Nhập charID lock", "charID" };
		list_0 = new List<int>();
		list_1 = new List<int>();
		string_2 = "2.0.6 - 06/04/2022 00:00:00";
		runSpeed = 8;
	}

	public static void Revive()
	{
		if (Char.myCharz().luong + Char.myCharz().luongKhoa > 0 && Char.myCharz().meDead && Char.myCharz().cHP <= 0 && GameCanvas.gameTick % 20 == 0)
		{
			Service.gI().wakeUpFromDead();
			Char.myCharz().meDead = false;
			Char.myCharz().statusMe = 1;
			Char.myCharz().cHP = Char.myCharz().cHPFull;
			Char.myCharz().cMP = Char.myCharz().cMPFull;
			Char @char = Char.myCharz();
			Char char2 = Char.myCharz();
			Char.myCharz().cp3 = 0;
			char2.cp2 = 0;
			@char.cp1 = 0;
			ServerEffect.addServerEffect(109, Char.myCharz(), 2);
			GameScr.gI().center = null;
			GameScr.isHaveSelectSkill = true;
		}
	}

	public static void FocusTo(int charId)
	{
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (!@char.isMiniPet && !@char.isPet && @char.charID == charId)
			{
				Char.myCharz().mobFocus = null;
				Char.myCharz().npcFocus = null;
				Char.myCharz().itemFocus = null;
				Char.myCharz().charFocus = @char;
				break;
			}
		}
	}

	public static bool isMeInNRDMap()
	{
		if (TileMap.mapID >= 85)
		{
			return TileMap.mapID <= 91;
		}
		return false;
	}

	public static void smethod_4()
	{
	}

	public static void smethod_5()
	{
	}

	public static bool smethod_6(int int_0)
	{
		return list_0.Contains(int_0);
	}

	public static void TeleportToFocus()
	{
		if (Char.myCharz().charFocus != null)
		{
			TeleportTo(Char.myCharz().charFocus.cx, Char.myCharz().charFocus.cy);
		}
		else if (Char.myCharz().itemFocus != null)
		{
			TeleportTo(Char.myCharz().itemFocus.x, Char.myCharz().itemFocus.y);
		}
		else if (Char.myCharz().mobFocus != null)
		{
			TeleportTo(Char.myCharz().mobFocus.x, Char.myCharz().mobFocus.y);
		}
		else if (Char.myCharz().npcFocus != null)
		{
			TeleportTo(Char.myCharz().npcFocus.cx, Char.myCharz().npcFocus.cy - 3);
		}
		else
		{
			GameScr.info1.addInfo("Không Có Mục Tiêu!", 0);
		}
	}

	public static bool isBoss(Char ch)
	{
		if (ch.cName != null && ch.cName != "" && !ch.isPet && !ch.isMiniPet && char.IsUpper(char.Parse(ch.cName.Substring(0, 1))) && ch.cName != "Trọng tài" && !ch.cName.StartsWith("#"))
		{
			return !ch.cName.StartsWith("$");
		}
		return false;
	}

	public static void EnterNRDMap()
	{
		if (isOpenMenuNPC && (TileMap.mapID == 24 || TileMap.mapID == 25 || TileMap.mapID == 26) && GameCanvas.gameTick % 20 == 0)
		{
			Service.gI().openMenu(29);
			Service.gI().confirmMenu(29, 1);
			if (GameCanvas.panel.mapNames != null && GameCanvas.panel.mapNames.Length > 6 && GameCanvas.panel.mapNames[mapIdNRD - 1] == nameMapsNRD[mapIdNRD - 1])
			{
				Service.gI().requestMapSelect(mapIdNRD - 1);
				isOpenMenuNPC = false;
			}
		}
		if (isMeInNRDMap() && !Char.isLoadingMap && !Controller.isStopReadMessage && GameCanvas.gameTick % 20 == 0)
		{
			Service.gI().requestChangeZone(zoneIdNRD, -1);
			isAutoEnterNRDMap = false;
			isOpenMenuNPC = true;
		}
	}

	public static void smethod_7()
	{
	}

	public static bool Chat(string text)
	{
		if (text.Equals(""))
		{
			return false;
		}
		if (text.StartsWith("k_"))
		{
			try
			{
				int zoneId = int.Parse(text.Split('_')[1]);
				Service.gI().requestChangeZone(zoneId, -1);
			}
			catch
			{
			}
			return true;
		}
		if (text.StartsWith("s_"))
		{
			try
			{
				int num = (runSpeed = int.Parse(text.Split('_')[1]));
				GameScr.info1.addInfo("Tốc Độ Di Chuyển: " + num, 0);
			}
			catch
			{
			}
			return true;
		}
		if (text.Equals("bdkb"))
		{
			Service.gI().confirmMenu(13, 0);
			return true;
		}
		if (text.StartsWith("delay_"))
		{
			try
			{
				delay = int.Parse(text.Split('_')[1]);
			}
			catch
			{
			}
			return true;
		}
		if (text.StartsWith("nrd_"))
		{
			try
			{
				int num2 = int.Parse(text.Split('_')[1]);
				int num3 = int.Parse(text.Split('_')[2]);
				mapIdNRD = num2;
				zoneIdNRD = num3;
				GameScr.info1.addInfo("NRD: " + mapIdNRD + "sk" + zoneIdNRD, 0);
			}
			catch
			{
			}
			return true;
		}
		if (text.StartsWith("cheat_"))
		{
			try
			{
				float num5 = (Time.timeScale = float.Parse(text.Split('_')[1]));
				GameScr.info1.addInfo("Cheat: " + num5, 0);
			}
			catch
			{
			}
			return true;
		}
		if (text.StartsWith("cheatf_"))
		{
			try
			{
				float num7 = (Time.timeScale = float.Parse(text.Split('_')[1]) / 10f);
				GameScr.info1.addInfo("Cheat: " + num7, 0);
			}
			catch
			{
			}
			return true;
		}
		return false;
	}

	public static void smethod_8()
	{
	}

	public static int MyHPPercent()
	{
		return (int)((long)Char.myCharz().cHP * 100L / Char.myCharz().cHPFull);
	}

	public static bool isMyHPLowerThan(int percent)
	{
		if (Char.myCharz().cHP > 0)
		{
			return MyHPPercent() < percent;
		}
		return false;
	}

	public static void smethod_9()
	{
	}

	public static void smethod_10()
	{
	}

	public static void smethod_11()
	{
	}

	public static int smethod_12()
	{
		return 2000000000;
	}

	public static string GetFlagName(int flagId)
	{
		if (flagId != -1 && flagId != 0)
		{
			string text = "";
			switch (flagId)
			{
			case 1:
				text = "Cờ xanh";
				break;
			case 2:
				text = "Cờ đỏ";
				break;
			case 3:
				text = "Cờ tím";
				break;
			case 4:
				text = "Cờ vàng";
				break;
			case 5:
				text = "Cờ lục";
				break;
			case 6:
				text = "Cờ hồng";
				break;
			case 7:
				text = "Cờ cam";
				break;
			case 8:
				text = "Cờ đen";
				break;
			case 9:
				text = "Cờ Kaio";
				break;
			case 10:
				text = "Cờ Mabu";
				break;
			case 11:
				text = "Cờ xanh dương";
				break;
			}
			if (!text.Equals(""))
			{
				return "(" + text + ") ";
			}
			return text;
		}
		return "";
	}

	public static void LoadData()
	{
		LoadFlagColor();
		isHuntingBoss = Rms.loadRMSInt("sanboss") == 1;
		isPaintBackground = Rms.loadRMSInt("sanboss") == 1;
		isShowCharsInMap = Rms.loadRMSInt("showchar") == 1;
		isReduceGraphics = Rms.loadRMSInt("IsReduceGraphics") == 1;
		if (mGraphics.zoomLevel == 2)
		{
			try
			{
				logoGameScreen = Image.__createImage(Convert.FromBase64String(LogoMod.logobase64origin));
				logoServerListScreen = Image.__createImage(Convert.FromBase64String(LogoMod.logobase64origin));
			}
			catch
			{
			}
		}
		if (mGraphics.zoomLevel == 1)
		{
			try
			{
				logoServerListScreen = Image.__createImage(Convert.FromBase64String(LogoMod.logobase64origin));
				logoGameScreen = Image.__createImage(Convert.FromBase64String(LogoMod.logobase64origin));
			}
			catch
			{
			}
		}
		try
		{
			APIKey = File.ReadAllText("Data\\keyAPI.ini");
			APIServer = File.ReadAllText("Data\\serverAPI.ini");
		}
		catch
		{
		}
	}

	public static void LoadFlagColor()
	{
		listFlagColor.Add(Color.black);
		listFlagColor.Add(new Color(0f, 0.99609375f, 0.99609375f));
		listFlagColor.Add(Color.red);
		listFlagColor.Add(new Color(0.54296875f, 0f, 0.54296875f));
		listFlagColor.Add(Color.yellow);
		listFlagColor.Add(Color.green);
		listFlagColor.Add(new Color(0.99609375f, 0.51171875f, 125f / 128f));
		listFlagColor.Add(new Color(0.80078125f, 51f / 128f, 0f));
		listFlagColor.Add(Color.black);
		listFlagColor.Add(Color.blue);
		listFlagColor.Add(Color.red);
		listFlagColor.Add(Color.blue);
	}

	public static void SortListCharsInMapByClan()
	{
		if (listCharsInMap.Count <= 2)
		{
			return;
		}
		List<Char> list = new List<Char>();
		while (listCharsInMap.Count != 0)
		{
			Char @char = listCharsInMap[0];
			list.Add(@char);
			string clanTag = @char.GetClanTag();
			listCharsInMap.RemoveAt(0);
			for (int i = 0; i < listCharsInMap.Count; i++)
			{
				Char char2 = listCharsInMap[i];
				if (clanTag == char2.GetClanTag())
				{
					list.Add(char2);
					listCharsInMap.RemoveAt(i);
					i--;
				}
			}
		}
		listCharsInMap.Clear();
		listCharsInMap = list;
	}

	public static void ConnectToAccountManager()
	{
		string path = Path.GetTempPath() + "koi occtiu957\\mod 222\\auto";
		if (mSystem.currentTimeMillis() - lastTimeConnected >= 3500 && File.Exists(path))
		{
			string text = File.ReadAllText(path);
			new Thread((ThreadStart)delegate
			{
				UseSkill(int.Parse(text));
			}).Start();
			lastTimeConnected = mSystem.currentTimeMillis();
		}
	}

	public static bool isColdImmune(Item item)
	{
		int id = item.template.id;
		if (id != 450 && id != 630 && id != 631 && id != 632 && id != 878 && id != 879)
		{
			if (id >= 386)
			{
				return id <= 394;
			}
			return false;
		}
		return true;
	}

	public static void UseCapsule()
	{
		isUsingCapsule = true;
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			Item item = Char.myCharz().arrItemBag[i];
			if (item != null && (item.template.id == 194 || item.template.id == 193))
			{
				Service.gI().useItem(0, 1, (sbyte)item.indexUI, -1);
				break;
			}
		}
		Thread.Sleep(500);
		Service.gI().requestMapSelect(0);
		Thread.Sleep(1000);
		isUsingCapsule = false;
	}

	public static void UseSkill(int skillIndex)
	{
		if (!isUsingSkill)
		{
			isUsingSkill = true;
			if (Char.myCharz().myskill != GameScr.keySkill[skillIndex])
			{
				GameScr.gI().doSelectSkill(GameScr.keySkill[skillIndex], isShortcut: true);
				Thread.Sleep(200);
				GameScr.gI().doSelectSkill(GameScr.keySkill[skillIndex], isShortcut: true);
				isUsingSkill = false;
			}
			else
			{
				GameScr.gI().doSelectSkill(GameScr.keySkill[skillIndex], isShortcut: true);
				isUsingSkill = false;
			}
		}
	}

	public static void SolveCapcha()
	{
		isSlovingCapcha = true;
		Thread.Sleep(1000);
		try
		{
			WebClient webClient = new WebClient();
			NameValueCollection data = new NameValueCollection
			{
				["merchant_key"] = APIKey,
				["type"] = "19",
				["image"] = Convert.ToBase64String(GameScr.imgCapcha.texture.EncodeToPNG())
			};
			Thread.Sleep(500);
			byte[] bytes = webClient.UploadValues(APIServer, data);
			string @string = Encoding.Default.GetString(bytes);
			Thread.Sleep(500);
			if (@string.Contains("\"message\":\"success\"") && @string.Contains("\"success\":true"))
			{
				string text = @string.Split(':')[3].Split('"')[1].Trim();
				Thread.Sleep(500);
				if (text.Length >= 4 && text.Length <= 7)
				{
					for (int i = 0; i < text.Length; i++)
					{
						Service.gI().mobCapcha(text[i]);
						Thread.Sleep(Res.random(500, 700));
					}
					Thread.Sleep(3000);
				}
			}
		}
		catch
		{
			Thread.Sleep(3000);
		}
		Thread.Sleep(1000);
		if (GameScr.gI().mobCapcha != null)
		{
			Thread.Sleep(3000);
		}
		isSlovingCapcha = false;
	}

	public static void smethod_13()
	{
	}

	public static bool isMeHasEnoughMP(Skill skillToUse)
	{
		if (skillToUse.template.manaUseType == 2)
		{
			return true;
		}
		if (skillToUse.template.manaUseType != 1)
		{
			return Char.myCharz().cMP >= skillToUse.manaUse;
		}
		return Char.myCharz().cMP >= skillToUse.manaUse * Char.myCharz().cMPFull / 100;
	}

	public static void smethod_14()
	{
	}

	public static void smethod_16()
	{
	}

	public static string DecryptString(string str, string key)
	{
		byte[] array = Convert.FromBase64String(str);
		byte[] key2 = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(key));
		byte[] bytes = new TripleDESCryptoServiceProvider
		{
			Key = key2,
			Mode = CipherMode.ECB,
			Padding = PaddingMode.PKCS7
		}.CreateDecryptor().TransformFinalBlock(array, 0, array.Length);
		return Encoding.UTF8.GetString(bytes);
	}

	public static void isMeCanAttack()
	{
		if (!AutoSkill.isAutoSendAttack)
		{
			AutoSkill.isAutoSendAttack = true;
		}
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (AutoSkill.isMeCanAttackChar(@char) && isBoss(@char) && @char.cHP <= selectHP && @char.cHP > 0 && @char.statusMe != 14 && @char.statusMe != 5)
			{
				Char.myCharz().charFocus = @char;
				if ((Math.abs(Char.myCharz().cx - @char.cx) > 40 || Math.abs(Char.myCharz().cy - @char.cy) > 40) && mSystem.currentTimeMillis() - timeCanAttack >= 500)
				{
					timeCanAttack = mSystem.currentTimeMillis();
					TeleportTo(@char.cx, GetYGround(@char.cx));
					Service.gI().charMove();
					Char.myCharz().cy = @char.cy;
					Service.gI().charMove();
				}
				break;
			}
		}
	}

	public static void ShowMenuBoss()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Tấn Công Boss\n" + (isAttackBoss ? "[STATUS: ON]" : "[STATUS: OFF]"), getInstance(), 19, null));
		myVector.addElement(new Command("Limit\n[" + NinjaUtil.getMoneys(selectHP) + "HP]", getInstance(), 20, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void OnChatFromMe(string str)
	{
		if (str.Contains("k "))
		{
			string[] parts = str.Split(' ');
			int index = Array.IndexOf(parts, "k");
			if (index != -1 && index + 1 < parts.Length)
			{
				int khu;
				if (int.TryParse(parts[index + 1], out khu))
				{
					Service.gI().requestChangeZone(khu, -1);
					str = "";
				}
			}
		}

		if (str.Equals("cs"))
        {
			for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
			{
				Item item = Char.myCharz().arrItemBag[i];
				if (item != null && (item.template.id == 194 || item.template.id == 193))
				{
					Service.gI().useItem(0, 1, (sbyte)item.indexUI, -1);
					break;
				}
			}
			str = "";
		}			

		if (str.Equals("ttdt"))
		{
			petw = !petw;
			GameScr.info1.addInfo("Pet View " + (petw ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			str = "";
		}
		if (str.Equals("ttsp"))
		{
			charw = !charw;
			GameScr.info1.addInfo("Char View " + (petw ? "[STATUS: ON] " : "[STATUS: OFF]"), 0);
			str = "";
		}
		if (str.Equals("viewmap"))
		{
			map = !map;
			GameScr.info1.addInfo("View Map: " + (map ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			str = "";
		}
		if (str.Equals("viewtime"))
		{
			time = !time;
			GameScr.info1.addInfo("View Time: " + (time ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			str = "";
		}
		if (str.Equals("viewname"))
		{
			name = !name;
			GameScr.info1.addInfo("View Name: " + (name ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			str = "";
		}
		if (str.Equals("viewsite"))
		{
			site = !site;
			GameScr.info1.addInfo("View Site: " + (site ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
			str = "";
		}
		if (str.Equals("vq"))
		{
			if (TileMap.mapID == 45)
			{
				AutoThuongDe.startmenu = !AutoThuongDe.startmenu;
				new Thread(AutoThuongDe.startMenu).Start();
				GameScr.info1.addInfo("Auto VQTD: " + (AutoThuongDe.startmenu ? "[STATUS:ON]" : "[STATUS:OFF]"), 0);
				return;
			}
			ChatPopup.addBigMessage("|0|Bạn Không Ở Thần Điện!\n|1|Bạn Có Muốn Đến Thần Điện Để Sử Dụng Auto Không?", 100000, new Npc(-1, 0, 0, 0, 0, 0)
			{
				avatar = 7992
			});
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command("Đến!", ChatPopup.serverChatPopUp, 9991, null);
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 75;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 35;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command("Không", ChatPopup.serverChatPopUp, 9992, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 11;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 35;
		}
	}

	public static void ShowMenuHienThi()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Thông tin", getInstance(), 39, null));
		myVector.addElement(new Command("Hiển Thị Map\n" + (map ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 35, null));
		myVector.addElement(new Command("Hiển Thị Time\n" + (time ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 36, null));
		myVector.addElement(new Command("Hiển Thị Site\n" + (site ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 37, null));
		myVector.addElement(new Command("Hiển Thị Name\n" + (name ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 38, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void infoView(mGraphics g, Char c, int xStart, int yStart)
	{
		mFont obj = ((!GameCanvas.lowGraphic) ? mFont.tahoma_7_red : mFont.tahoma_7_red);
		mFont font = ((!GameCanvas.lowGraphic) ? mFont.tahoma_7 : mFont.tahoma_7_blue);
		obj.drawString(g, "Tên: " + c.cName, xStart, yStart + 14, 0, font);
		obj.drawString(g, "HP: " + NinjaUtil.getMoneys(c.cHP) + " / " + NinjaUtil.getMoneys(c.cHPFull), xStart, yStart + 28, 0, font);
		obj.drawString(g, "MP: " + NinjaUtil.getMoneys(c.cMP) + " / " + NinjaUtil.getMoneys(c.cMPFull), xStart, yStart + 42, 0, font);
		obj.drawString(g, "SĐ: " + NinjaUtil.getMoneys(c.cDamFull), xStart, yStart + 56, 0, font);
		obj.drawString(g, "SM: " + NinjaUtil.getMoneys(c.cPower), xStart, yStart + 70, 0, font);
		obj.drawString(g, "TN: " + NinjaUtil.getMoneys(c.cTiemNang), xStart, yStart + 84, 0, font);
	}

	public static void ShowMenuThongTin()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Thông tin Sư phụ" + (charw ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 40, null));
		myVector.addElement(new Command("Thông Tin Đệ tử" + (petw ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 41, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuAutoItem()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Item Cấp 2", getInstance(), 56, null));
		myVector.addElement(new Command("Auto Cuồng Nộ" + (AutoSuDungItem.cn ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 47, null));
		myVector.addElement(new Command("Auto Bổ Huyết" + (AutoSuDungItem.bh ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 48, null));
		myVector.addElement(new Command("Auto Bổ Khí" + (AutoSuDungItem.bk ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 49, null));
		myVector.addElement(new Command("Auto Giáp Xên" + (AutoSuDungItem.gx ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 50, null));
		myVector.addElement(new Command("Auto Ẩn Danh" + (AutoSuDungItem.ad ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 51, null));
		myVector.addElement(new Command("Auto Máy Dò" + (AutoSuDungItem.md ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 52, null));
		myVector.addElement(new Command("Auto CSKB" + (AutoSuDungItem.cskb ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 53, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuAutoMore()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Auto Đập Đồ" + (UpgradeItem.isDapDo ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 54, null));
		myVector.addElement(new Command("Auto VQTD" + (AutoThuongDe.startmenu ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 55, null));
		myVector.addElement(new Command("Tự động tấn công Boss", getInstance(), 18, null));
		myVector.addElement(new Command("Tàn sát người", getInstance(), 21, null));
		myVector.addElement(new Command("Khung Char\n" + (linechar ? "[STATUS:ON]" : "STATUS:OFF"), getInstance(), 77, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuAutoUp()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Auto Yadrat" + (AutoYardrat.isautobikiep ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 61, null));
		myVector.addElement(new Command("TDLT" + (Tugino.isTDLT ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 62, null));
		myVector.addElement(new Command("Auto Bông Tai" + (AutoSuDungItem.isAutoBT ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 63, null));
		myVector.addElement(new Command("Auto TTNL" + (Tugino.isAutoTTNL ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 67, null));
		myVector.addElement(new Command("Auto Hồi Sinh" + (Tugino.isAutoHS ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 68, null));
		myVector.addElement(new Command("Khóa Vị Trí" + (Tugino.isKhoaViTri ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 69, null));
		myVector.addElement(new Command("Khóa Khu" + (Tugino.isKhoaKhu ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 70, null));
		myVector.addElement(new Command("Auto Up Kaioken" + (Tugino.isKOK ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 71, null));
		myVector.addElement(new Command("Auto Flag" + (Tugino.isAutoCo ? "\n[STATUS:ON]" : "\n[STATUS:OFF]"), getInstance(), 72, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuChucNang()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Hiển Thị", getInstance(), 31, null));
		myVector.addElement(new Command("Auto Item", getInstance(), 43, null));
		myVector.addElement(new Command("Auto Screen", getInstance(), 44, null));
		myVector.addElement(new Command("Auto More", getInstance(), 45, null));
		myVector.addElement(new Command("Auto Up", getInstance(), 46, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	public static void ShowMenuItemCap2()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Auto Cuồng Nộ C2" + (AutoSuDungItem.cnc2 ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 57, null));
		myVector.addElement(new Command("Auto Bổ Huyết C2" + (AutoSuDungItem.bhc2 ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 58, null));
		myVector.addElement(new Command("Auto Bổ Khí C2" + (AutoSuDungItem.bkc2 ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 59, null));
		myVector.addElement(new Command("Auto Giáp Xên C2" + (AutoSuDungItem.gxc2 ? "\n[STATUS: ON] " : "\n[STATUS: OFF]"), getInstance(), 60, null));
		GameCanvas.menu.startAt(myVector, 3);
	}

	private int findSkillIndex(int id)
	{
		int num = 0;
		Skill[] keySkill = GameScr.keySkill;
		foreach (Skill skill in keySkill)
		{
			if (skill != null && skill.template.id == id)
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	public static void ShowMenuAutoScreen()
	{
		MyVector myVector = new MyVector();
		myVector.addElement(new Command("Boss Trong Khu\n" + (bosstrongkhu ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 64, null));
		myVector.addElement(new Command("Auto Login\n" + (isAutoLogin ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 65, null));
		myVector.addElement(new Command("Ẩn Logo\n" + (islogo ? "[STATUS: OFF] " : "[STATUS: ON]"), getInstance(), 66, null));
		myVector.addElement(new Command("Delete Map\n" + (GameScr.xoamap ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 73, null));
		myVector.addElement(new Command("Delete All\n" + (GameScr.hideInterface ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 74, null));
		myVector.addElement(new Command("Black Background\n" + (GameCanvas.troiden ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 75, null));
		myVector.addElement(new Command("Dark Background\n" + (GameScr.isRongThanXuatHien ? "[STATUS: ON] " : "[STATUS: OFF]"), getInstance(), 76, null));
		GameCanvas.menu.startAt(myVector, 3);
	}
}
