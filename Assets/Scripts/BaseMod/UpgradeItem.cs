using System.Threading;
using UnityEngine;

public class UpgradeItem
{
	public static bool isDapDo;

	public static Item doDeDap;

	public static int soSaoCanDap;

	private static int saoHienTai;

	private static bool dangBanVang;

	public static bool isThuongDeThuong;

	public static bool isThuongDeVip;

	public static void AutoDapDo()
	{
		while (isDapDo)
		{
			if (Input.GetKey("q"))
			{
				GameScr.info1.addInfo("Đồ để đập đã reset hãy add đồ", 0);
				soSaoCanDap = -1;
				doDeDap = null;
			}
			if (TileMap.mapID != 5 && !AutoMap.isXmaping)
			{
				AutoMap.StartRunToMapId(5);
			}
			while (TileMap.mapID != 5)
			{
				Thread.Sleep(100);
			}
			if (saoHienTai >= soSaoCanDap && doDeDap != null && saoHienTai >= 0 && soSaoCanDap > 0)
			{
				Sound.start(1f, Sound.l1);
				GameScr.info1.addInfo("Đồ Cần Đập Đã Đạt Số Sao Yêu Cầu", 0);
				soSaoCanDap = -1;
				doDeDap = null;
			}
			if (Char.myCharz().xu > 200000000)
			{
				_ = Char.myCharz().xu;
				GotoNpc(21);
				if (doDeDap != null && soSaoCanDap > 0)
				{
					while (!GameCanvas.menu.showMenu)
					{
						Service.gI().combine(1, GameCanvas.panel.vItemCombine);
						Thread.Sleep(500);
					}
					Service.gI().confirmMenu(21, 0);
					GameCanvas.menu.doCloseMenu();
					GameCanvas.panel.currItem = null;
					GameCanvas.panel.chatTField.isShow = false;
				}
			}
			else if (doDeDap != null)
			{
				BanVang();
			}
			Thread.Sleep(100);
		}
	}

	public static void GotoNpc(int npcID)
	{
		for (int i = 0; i < GameScr.vNpc.size(); i++)
		{
			Npc npc = (Npc)GameScr.vNpc.elementAt(i);
			if (npc.template.npcTemplateId == npcID && Math.abs(npc.cx - Char.myCharz().cx) >= 50)
			{
				GotoXY(npc.cx, npc.cy - 1);
				Char.myCharz().focusManualTo(npc);
				break;
			}
		}
	}

	public static void GotoXY(int x, int y)
	{
		Char.myCharz().cx = x;
		Char.myCharz().cy = y;
		Service.gI().charMove();
	}

	public static void BanVang()
	{
		dangBanVang = true;
		if (TileMap.mapID != 5)
		{
			AutoMap.StartRunToMapId(5);
			Thread.Sleep(1000);
		}
		while (TileMap.mapID != 5)
		{
			Thread.Sleep(500);
		}
		if (Input.GetKey("q"))
		{
			GameScr.info1.addInfo("Dừng bán vàng", 0);
			dangBanVang = false;
			return;
		}
		while (Char.myCharz().xu <= 1500000000 && !Input.GetKey("q"))
		{
			if (thoiVang() <= 0)
			{
				GameScr.info1.addInfo("Hết vàng", 0);
				if (isDapDo)
				{
					isDapDo = false;
					GameScr.info1.addInfo("Đập đồ đã tắt do bạn quá nghèo :v", 0);
				}
				dangBanVang = false;
				return;
			}
			Service.gI().saleItem(1, 1, (short)FindIndexItem(457));
			Thread.Sleep(500);
			Thread.Sleep(500);
		}
		GameScr.info1.addInfo("Đã bán xong", 0);
		Thread.Sleep(500);
		dangBanVang = false;
	}

	public static int FindIndexItem(int idItem)
	{
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			if (Char.myCharz().arrItemBag[i] != null && Char.myCharz().arrItemBag[i].template.id == idItem)
			{
				return Char.myCharz().arrItemBag[i].indexUI;
			}
		}
		return -1;
	}

	public static int thoiVang()
	{
		int num = 0;
		for (int i = 0; i < Char.myCharz().arrItemBag.Length; i++)
		{
			Item item = Char.myCharz().arrItemBag[i];
			if (item != null && item.template.id == 457)
			{
				num += item.quantity;
			}
		}
		return num;
	}

	static UpgradeItem()
	{
		soSaoCanDap = -1;
		saoHienTai = -1;
	}

	public static void chat(string str)
	{
		if (str.Equals("dapdo"))
		{
			isDapDo = !isDapDo;
			new Thread(AutoDapDo).Start();
			GameScr.info1.addInfo("Đập đồ: " + (isDapDo ? "Bật" : "Tắt"), 0);
			str = "";
		}
	}

	public static void paint(mGraphics g)
	{
		if (isDapDo)
		{
			mFont.tahoma_7b_red.drawString(g, (doDeDap != null) ? doDeDap.template.name : "Chưa Có", GameCanvas.w / 2, 72, mFont.CENTER);
			mFont.tahoma_7b_red.drawString(g, (doDeDap != null) ? ("Số Sao : " + saoHienTai) : "Số Sao : -1", GameCanvas.w / 2, 82, mFont.CENTER);
			mFont.tahoma_7b_red.drawString(g, "Số Sao Cần Đập : " + soSaoCanDap + " Sao", GameCanvas.w / 2, 92, mFont.CENTER);
		}
		if (isDapDo || isThuongDeThuong || isThuongDeVip)
		{
			mFont.tahoma_7b_red.drawString(g, "Ngọc Xanh : " + NinjaUtil.getMoneys(Char.myCharz().luong) + " Ngọc Hồng : " + NinjaUtil.getMoneys(Char.myCharz().luongKhoa), GameCanvas.w / 2, 102, mFont.CENTER);
			mFont.tahoma_7b_red.drawString(g, "Vàng : " + NinjaUtil.getMoneys(Char.myCharz().xu) + " Thỏi Vàng : " + thoiVang(), GameCanvas.w / 2, 112, mFont.CENTER);
		}
	}
}
