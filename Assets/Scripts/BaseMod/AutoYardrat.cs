using System;
using System.Threading;
public class AutoYardrat
{
	public static AutoYardrat instance;

	public static bool isautobikiep = false;

	public static long lastTimeNext = 0L;

	public static bool ispause = false;

	public static int ttnlIndex = -1;

	public static int khienIndex = -1;

	public bool canUseSkill(Skill skill)
	{
		if (skill == null)
		{
			return false;
		}
		int num = ((skill.template.manaUseType == 2) ? 1 : ((skill.template.manaUseType == 1) ? (skill.manaUse * Char.myCharz().cMPFull / 100) : skill.manaUse));
		return mSystem.currentTimeMillis() - skill.lastTimeUseThisSkill > skill.coolDown && Char.myCharz().cMP >= num;
	}

	public static AutoYardrat gI()
	{
		if (instance == null)
		{
			return instance = new AutoYardrat();
		}
		return instance;
	}

	public static void update()
	{
		while (isautobikiep)
		{
			try
			{
				if (isautobikiep)
				{
					if (khienIndex != -1 && gI().canUseSkill(GameScr.keySkill[khienIndex]) && !Char.myCharz().isCharge)
					{
						GameScr.gI().doSelectSkill(GameScr.keySkill[khienIndex], isShortcut: true);
						GameScr.gI().doSelectSkill(GameScr.keySkill[khienIndex], isShortcut: true);
						if (GameScr.keySkill[2].coolDown > 1500)
						{
							GameScr.gI().auto = 10;
						}
						Thread.Sleep(500);
						GameScr.gI().doSelectSkill(GameScr.keySkill[0], isShortcut: true);
					}
					if (AutoPick.isAutoPick && AutoPick.isAutoPick && GameCanvas.gameTick % 20 == 0)
					{
						isautobikiep = true;
					}
					if ((Char.myCharz().isNhapThe || isautobikiep) && Char.myCharz().cHP <= 50000)
					{
						while (true)
						{
							for (int i = 0; i <= 50000; i++)
							{
								MainMod.UseItem(457);
								MainMod.UseItem(921);
							}
						}
					}
					if (Char.myCharz().cgender == 2)
					{
						if (((double)Char.myCharz().cHP <= (double)Char.myCharz().cHPFull * 0.4 || (double)Char.myCharz().cMP <= (double)Char.myCharz().cMPFull * 0.3) && ttnlIndex != -1 && gI().canUseSkill(GameScr.keySkill[ttnlIndex]))
						{
							GameScr.gI().doSelectSkill(GameScr.keySkill[ttnlIndex], isShortcut: true);
							GameScr.gI().doSelectSkill(GameScr.keySkill[ttnlIndex], isShortcut: true);
							if (GameScr.keySkill[2].coolDown > 1500)
							{
								GameScr.gI().auto = 10;
							}
							ispause = true;
						}
						Thread.Sleep(500);
					}
					if (!Char.myCharz().isMeCanAttackOtherPlayer(Char.myCharz().charFocus) || mSystem.currentTimeMillis() - lastTimeNext > 5000 || Char.myCharz().charFocus == null)
					{
						Char.myCharz().findNextFocusByKey();
						lastTimeNext = mSystem.currentTimeMillis();
					}
					if (!Char.myCharz().isCharge && ispause && Char.myCharz().myskill != GameScr.keySkill[0])
					{
						GameScr.gI().doSelectSkill(GameScr.keySkill[0], isShortcut: true);
						ispause = false;
					}
				}
				Thread.Sleep(100);
			}
			catch (Exception)
			{
			}
		}
	}
}
