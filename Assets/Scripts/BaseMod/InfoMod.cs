
public class InfoMod : IActionListener
{
	public static InfoMod info;

	public static InfoMod gI()
	{
		if (info == null)
		{
			info = new InfoMod();
		}
		return info;
	}

	public static void Info(Npc NPC, string Text, string text1, string text2, IActionListener idAction, int idAction1, int idAction2, int Type, int Button)
	{
		if (Type == 0)
		{
			ChatPopup.addBigMessage(Text, 0, NPC);
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command(text1, idAction, idAction1, null);
		}
		if (Type == 1)
		{
			NPC = new Npc(0, 0, 0, 0, 0, 0);
			ChatPopup.addBigMessage(Text, 0, NPC);
			ChatPopup.serverChatPopUp.cmdMsg1 = new Command(text1, idAction, idAction1, null);
		}
		if (Button == 1)
		{
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 38;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 33;
		}
		if (Button == 2)
		{
			ChatPopup.serverChatPopUp.cmdMsg1.x = GameCanvas.w / 2 - 77;
			ChatPopup.serverChatPopUp.cmdMsg1.y = GameCanvas.h - 33;
			ChatPopup.serverChatPopUp.cmdMsg2 = new Command(text2, idAction, idAction2, null);
			ChatPopup.serverChatPopUp.cmdMsg2.x = GameCanvas.w / 2 + 2;
			ChatPopup.serverChatPopUp.cmdMsg2.y = GameCanvas.h - 33;
		}
	}

	public void perform(int idAction, object p)
	{
		if (idAction == 1)
		{
			EndInfo();
		}
		if (idAction == 2)
		{
			GameCanvas.endDlg();
		}
		if (idAction == 4)
		{
			Main.exit();
		}
		if (idAction == 5)
		{
			if (Rms.loadRMSInt(InfoText.lowGraphic) == 1)
			{
				Rms.saveRMSInt(InfoText.lowGraphic, 0);
			}
			else
			{
				Rms.saveRMSInt(InfoText.lowGraphic, 1);
			}
			Main.exit();
		}
	}

	public static void EndInfo()
	{
		ChatPopup.scr = null;
		Char.chatPopup = null;
		ChatPopup.serverChatPopUp = null;
		GameScr.info1.isUpdate = true;
		Char.isLockKey = false;
		if (ChatPopup.isHavePetNpc)
		{
			GameScr.info1.info.time = 0;
			GameScr.info1.info.info.speed = 10;
		}
	}
}
