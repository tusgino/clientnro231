﻿using System.Collections.Generic;

public class PaintCharInMap
{
	public static List<Char> chars = new List<Char>();

	public static bool ShowChar;

	public static PaintCharInMap instance;

	public static int X;

	public static int Y;

	public static PaintCharInMap gI()
	{
		if (instance == null)
		{
			instance = new PaintCharInMap();
		}
		return instance;
	}

	public static void Update()
	{
		chars.Clear();
		for (int i = 0; i < GameScr.vCharInMap.size(); i++)
		{
			Char @char = (Char)GameScr.vCharInMap.elementAt(i);
			if (!@char.isPet && !@char.isMiniPet && !@char.cName.StartsWith("#") && !@char.cName.StartsWith("$") && @char.cName != "Trọng tài")
			{
				chars.Add(@char);
			}
		}
		Check();
	}

	public static void Check()
	{
		if (chars.Count <= 2)
		{
			return;
		}
		List<Char> list = new List<Char>();
		while (chars.Count != 0)
		{
			Char @char = chars[0];
			list.Add(@char);
			string text = @char.CharCheck();
			chars.RemoveAt(0);
			for (int i = 0; i < chars.Count; i++)
			{
				Char char2 = chars[i];
				if (text == char2.CharCheck())
				{
					list.Add(char2);
					chars.RemoveAt(i);
					i--;
				}
			}
		}
		chars.Clear();
		chars = list;
	}

	public static bool MapNro()
	{
		if (TileMap.mapID >= 85)
		{
			return TileMap.mapID <= 91;
		}
		return false;
	}

	public static bool CheckCharName(Char a)
	{
		if (!a.isPet && !a.isMiniPet && char.IsUpper(char.Parse(a.cName.Substring(0, 1))) && a.cName != "Trọng tài" && !a.cName.StartsWith("#"))
		{
			return !a.cName.StartsWith("$");
		}
		return false;
	}

	public static void PaintFlag(Char @char, mGraphics g, int x, int y)
	{
		switch (@char.cFlag)
		{
			case 1:
				g.setColor(7468284);
				g.fillRect(x, y, 7, 7);
				break;
			case 2:
				g.setColor(16711680);
				g.fillRect(x, y, 7, 7);
				break;
			case 3:
				g.setColor(10895840);
				g.fillRect(x, y, 7, 7);
				break;
			case 4:
				g.setColor(14925336);
				g.fillRect(x, y, 7, 7);
				break;
			case 5:
				g.setColor(6406954);
				g.fillRect(x, y, 7, 7);
				break;
			case 6:
				g.setColor(15163872);
				g.fillRect(x, y, 7, 7);
				break;
			case 7:
				g.setColor(15362839);
				g.fillRect(x, y, 7, 7);
				break;
			case 8:
				g.setColor(0);
				g.fillRect(x, y, 7, 7);
				break;
		}
	}

	public static void Paint(mGraphics a)
	{
		int num = (MapNro() ? 35 : 95);
		X = 110;
		Y = 7;
		if (mGraphics.zoomLevel == 1)
		{
			X = 132;
			Y = 9;
		}
		for (int i = 0; i < chars.Count; i++)
		{
			Char @char = chars[i];
			a.setColor(2721889, 0.5f);
			a.fillRect(GameCanvas.w - X, num + 2, X - 2, Y);
			PaintFlag(@char, a, GameCanvas.w - X - 9, num + 2);
			if (!@char.isPet && !@char.isMiniPet && !@char.cName.StartsWith("#") && !@char.cName.StartsWith("$") && @char.cName != "Trọng tài")
			{
				string text = string.Concat(new object[4]
				{
					@char.cName,
					" [",
					NinjaUtil.getMoneys(@char.cHP),
					"]"
				});
				bool flag;
				if (!(flag = CheckCharName(@char)))
				{
					text = string.Concat(new object[6]
					{
						@char.cName,
						" [",
						NinjaUtil.getMoneys(@char.cHP),
						" - ",
						@char.HanhTinh(),
						"]"
					});
				}
				if (Char.myCharz().charFocus != null && Char.myCharz().charFocus.cName == @char.cName)
				{
					a.setColor(14155776);
					a.drawLine(Char.myCharz().cx - GameScr.cmx, Char.myCharz().cy - GameScr.cmy + 1, @char.cx - GameScr.cmx, @char.cy - GameScr.cmy);
					mFont.tahoma_7b_red.drawString(a, i + 1 + ". " + text, GameCanvas.w - X + 2, num, 0);
				}
				else if (flag)
				{
					a.setColor(16383818);
					a.drawLine(Char.myCharz().cx - GameScr.cmx, Char.myCharz().cy - GameScr.cmy + 1, @char.cx - GameScr.cmx, @char.cy - GameScr.cmy);
					mFont.tahoma_7b_red.drawString(a, i + 1 + ". " + text, GameCanvas.w - X + 2, num, 0);
				}
				else
				{
					mFont.tahoma_7.drawString(a, i + 1 + ". " + text, GameCanvas.w - X + 2, num, 0);
				}
				num += Y + 1;
			}
		}
	}
}
